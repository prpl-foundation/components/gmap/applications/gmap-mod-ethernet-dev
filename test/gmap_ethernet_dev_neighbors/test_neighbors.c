/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include "gmap_eth_dev.h"
#include "gmap_eth_dev_neighbors.h"
#include "amxd/amxd_object_event.h"
#include "amxo/amxo.h"
#include "amxd/amxd_object.h"
#include "amxd/amxd_transaction.h"

#include "../common/test-setup.h"
#include "../common/test-util.h"
#include "../common/mock_gmap.h"
#include "../common/mock_discoping.h"
#include "test_neighbors.h"
#include <arpa/inet.h>
#include <string.h>

static void s_expect_dev_create(const char* mac, const char* key, uint32_t family, bool already_exists) {
    const char* expected_tags = NULL;
    const char* expected_set_tags = NULL;

    switch(family) {
    case AF_INET:
        expected_tags = "lan edev mac physical ipv4";
        expected_set_tags = "ipv4";
        break;
    case AF_INET6:
        expected_tags = "lan edev mac physical ipv6";
        expected_set_tags = "ipv6";
        break;
    default:
        assert_true(false);
        break;
    }

    mock_gmap_expect_createDeviceOrGetKey(mac, (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = key,
        .will_return_already_exists = already_exists,
        .will_return_success = true,
        .expected_tags = expected_tags,
        .expected_discovery_source = "neigh",
    }));
    if(already_exists) {
        mock_gmap_expect_setTag(key, expected_set_tags, NULL, gmap_traverse_this);
    }
}


static void s_emit_created(amxd_object_t* object) {
    amxc_var_t event_data;
    amxc_var_t* neigh_data = NULL;
    char* ip_state = NULL;
    char* value = NULL;

    amxc_var_init(&event_data);

    ip_state = amxd_object_get_cstring_t(object, "State", NULL);
    if((ip_state == NULL) ||
       (strcmp(ip_state, "probe") == 0) ||
       (strcmp(ip_state, "delay") == 0) ||
       (strcmp(ip_state, "incomplete") == 0)) {
        print_message("Note: not emitting 'Neighbor!': Unknown or ignored state '%s'",
                      ip_state == NULL ? "(char*)0" : ip_state);
        goto exit;
    }

    amxc_var_set_type(&event_data, AMXC_VAR_ID_HTABLE);
    neigh_data = amxc_var_add_new_key(&event_data, "NeighborData");
    amxc_var_set_type(neigh_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, neigh_data, "State", ip_state);
    amxc_var_add_key(cstring_t, neigh_data, "OldState", "none");

    value = amxd_object_get_cstring_t(object, "Family", NULL);
    amxc_var_add_key(cstring_t, neigh_data, "Family", value);
    free(value);

    value = amxd_object_get_cstring_t(object, "Flags", NULL);
    amxc_var_add_key(cstring_t, neigh_data, "Flags", value);
    free(value);

    value = amxd_object_get_cstring_t(object, "Dst", NULL);
    amxc_var_add_key(cstring_t, neigh_data, "Dst", value);
    free(value);

    value = amxd_object_get_cstring_t(object, "LLAddress", NULL);
    amxc_var_add_key(cstring_t, neigh_data, "LLAddress", value);
    free(value);
    value = NULL;

    amxd_object_emit_signal(object, "Neighbor!", &event_data);

exit:
    amxc_var_clean(&event_data);
    free(ip_state);
}

static void s_emit_bridge_created(const char* bridge) {
    amxd_object_t* neigh = amxd_dm_findf(get_dummy_dm(), "NetDev.Link.%s.Neigh.", bridge);
    amxd_object_iterate(instance, it, neigh) {
        amxd_object_t* child = amxc_container_of(it, amxd_object_t, it);
        s_emit_created(child);
    }
}

static void s_emit_all_created(void) {
    s_emit_bridge_created("bridge1");
    s_emit_bridge_created("bridge2");
    s_emit_bridge_created("bridge3");
}

typedef struct neighbors_test_instance {
    mock_discoping_t discoping;
    gmap_eth_dev_discoping_t* discoping_gmap;
    gmap_eth_dev_bridge_collection_t bridges;
    gmap_eth_dev_bridge_t bridge;
} neighbors_test_instance_t;

static neighbors_test_instance_t* s_setup_neighbors(const char* netdev_name,
                                                    uint32_t netdev_index,
                                                    const char* mac,
                                                    uint32_t protocol_family,
                                                    const char* address) {
    neighbors_test_instance_t* instance = calloc(1, sizeof(neighbors_test_instance_t));
    assert_non_null(instance);

    instance->discoping_gmap = mock_discoping_new(&instance->discoping);
    instance->bridge = (gmap_eth_dev_bridge_t) {
        .collection = &instance->bridges,
        .netdev = (char*) netdev_name,
        .netdev_index = netdev_index,
        .mac = (char*) mac,
        .proto_family = protocol_family,
        .address = (char*) address,
        .discoping = instance->discoping_gmap,
    };
    gmap_eth_dev_neighbors_new(&instance->bridge.neighbors, &instance->bridge);
    assert_non_null(instance->bridge.neighbors);

    return instance;
}

static void s_clean_neighbors(neighbors_test_instance_t** instance) {
    assert_non_null(instance);
    assert_non_null(*instance);
    assert_non_null((*instance)->discoping_gmap);
    assert_non_null((*instance)->bridge.neighbors);

    gmap_eth_dev_neighbors_delete(&((*instance)->bridge.neighbors));
    assert_null((*instance)->bridge.neighbors);
    mock_discoping_delete(&((*instance)->discoping_gmap), &((*instance)->discoping));
    assert_null((*instance)->discoping_gmap);

    free(*instance);
    *instance = NULL;
}

void test_gmap_eth_dev_neighbors_new__invalid_argument(UNUSED void** state) {
    mock_discoping_t mock_ext_discoping = {};
    gmap_eth_dev_discoping_t* discoping = (gmap_eth_dev_discoping_t*) &mock_ext_discoping;
    gmap_eth_dev_neighbors_t* neighbors = NULL;

    gmap_eth_dev_bridge_t mock_bridge_empty = { 0 };
    gmap_eth_dev_bridge_collection_t mock_bridges = { 0 };
    gmap_eth_dev_bridge_t mock_bridge = {
        .collection = &mock_bridges,
        .netdev = (char*) "bridge_netdev",
        .netdev_index = 4,
        .mac = (char*) "60:50:40:30:20:10",
        .proto_family = AF_INET,
        .address = (char*) "192.168.1.1",
        .discoping = discoping,
    };

    // Case: NULL neighbors pointer
    gmap_eth_dev_neighbors_new(NULL, &mock_bridge);
    assert_null(neighbors);

    // Case: NULL bridge pointer
    assert_non_null(discoping);
    gmap_eth_dev_neighbors_new(&neighbors, NULL);
    assert_null(neighbors);

    // Case: Empty bridge pointer
    assert_non_null(discoping);
    gmap_eth_dev_neighbors_new(&neighbors, &mock_bridge_empty);
    assert_null(neighbors);
}

void test_gmap_eth_dev_neighbors__startup_entries_ipv4(UNUSED void** state) {
    amxc_var_t* ip_found = NULL;
    neighbors_test_instance_t* neighbor = NULL;

    // GIVEN netdev with populated bridgetables
    test_util_parse_odl("netdev-dm.odl");
    test_util_parse_odl("netdev-mockdata-bridges.odl");
    test_util_parse_odl("netdev-mockdata-neigh.odl");
    test_util_handle_events();

    // EXPECT devices (that have an IPv4 address) will be created in gmap
    s_expect_dev_create("00:00:00:00:00:01", "id1", AF_INET, false);
    s_expect_dev_create("00:00:00:00:00:04", "id4", AF_INET, false);
    s_expect_dev_create("00:00:00:00:00:05", "id5", AF_INET, false);
    s_expect_dev_create("00:00:00:00:00:06", "id6", AF_INET, true);

    // EXPECT IPv4 that exists will not be verified nor added
    //        and IPv4 addresses that do not exist in gmap, will be verified
    // id1:
    amxc_var_new(&ip_found);
    amxc_var_set_type(ip_found, AMXC_VAR_ID_HTABLE);
    mock_gmap_expect_ip_device_get_address("id1", AF_INET, "192.168.1.1", ip_found);
    // id4:
    mock_gmap_expect_ip_device_get_address("id4", AF_INET, "192.168.1.4", NULL);
    mock_discoping_expect_verify("192.168.1.4", "00:00:00:00:00:04");
    // id5:
    amxc_var_new(&ip_found);
    amxc_var_set_type(ip_found, AMXC_VAR_ID_HTABLE);
    mock_gmap_expect_ip_device_get_address("id5", AF_INET, "192.168.1.5", ip_found);
    // id6:
    mock_gmap_expect_ip_device_get_address("id6", AF_INET, "192.168.1.6", NULL);
    mock_discoping_expect_verify("192.168.1.6", "00:00:00:00:00:06");

    // WHEN starting gmap-mod-ethernet-dev
    test_setup_component_init(state);
    test_util_handle_events();
    neighbor = s_setup_neighbors("bridge1", 2,
                                 "60:50:40:30:20:10",
                                 AF_INET,
                                 "192.168.1.1");
    test_util_handle_events();
    s_clean_neighbors(&neighbor);
}

void test_gmap_eth_dev_neighbors__startup_entries_ipv6(UNUSED void** state) {
    neighbors_test_instance_t* neighbor = NULL;

    // GIVEN netdev with populated bridgetables
    test_util_parse_odl("netdev-dm.odl");
    test_util_parse_odl("netdev-mockdata-bridges.odl");
    test_util_parse_odl("netdev-mockdata-neigh.odl");
    test_util_handle_events();

    // EXPECT devices will be created in gmap
    s_expect_dev_create("00:00:00:00:00:08", "id8", AF_INET6, false);
    s_expect_dev_create("00:00:00:00:00:10", "id10", AF_INET6, true);
    s_expect_dev_create("00:00:00:00:00:03", "id3", AF_INET6, false);

    // EXPECT all IPv4 to be ignored

    // EXPECT IPv6 will be verified, regardless of whether the IP already exists in gmap
    // ip of id8:
    mock_discoping_expect_verify_once("2a02:1802:94:4200:211:11ff:fe01:2408", "00:00:00:00:00:08");
    // ip of id10:
    mock_discoping_expect_verify_once("2a02:1802:94:4200:211:11ff:fe01:2410", "00:00:00:00:00:10");
    // ip of id3:
    mock_discoping_expect_verify_once("fe80::6e48:ac89:9e00:4303", "00:00:00:00:00:03");

    // WHEN starting gmap-mod-ethernet-dev
    test_setup_component_init(state);
    test_util_handle_events();
    neighbor = s_setup_neighbors("bridge1", 2,
                                 "60:50:40:30:20:10",
                                 AF_INET6,
                                 "fe80::5877:88ff:fe48:3702");
    test_util_handle_events();
    s_clean_neighbors(&neighbor);
}

void test_gmap_eth_dev_neighbors__startup_entries_other_bridge(UNUSED void** state) {
    neighbors_test_instance_t* neighbor = NULL;

    // GIVEN netdev with populated bridgetables
    test_util_parse_odl("netdev-dm.odl");
    test_util_parse_odl("netdev-mockdata-bridges.odl");
    test_util_parse_odl("netdev-mockdata-neigh.odl");
    test_util_handle_events();

    // EXPECT devices will be created in gmap
    s_expect_dev_create("00:00:00:00:00:07", "id7", AF_INET, false);

    // EXPECT IPv4 that does not exist in gmap, will be verified
    // ip of id7:
    mock_gmap_expect_ip_device_get_address("id7", AF_INET, "169.254.1.7", NULL);
    mock_discoping_expect_verify("169.254.1.7", "00:00:00:00:00:07");

    // WHEN starting gmap-mod-ethernet-dev
    test_setup_component_init(state);
    test_util_handle_events();
    neighbor = s_setup_neighbors("bridge3", 3,
                                 "60:50:40:30:20:10",
                                 AF_INET,
                                 "192.168.1.2");
    test_util_handle_events();
    s_clean_neighbors(&neighbor);
}

void test_gmap_eth_dev_neighbors__notification_new_entries_ipv4(void** state) {
    neighbors_test_instance_t* neighbor = NULL;
    amxc_var_t* ip_found = NULL;

    // GIVEN a gmap-mod-ethernet-dev and a netdev with empty bridgetables
    test_util_parse_odl("netdev-dm.odl");
    test_util_parse_odl("netdev-mockdata-bridges.odl");
    test_setup_component_init(state);
    neighbor = s_setup_neighbors("bridge1", 2,
                                 "60:50:40:30:20:10",
                                 AF_INET,
                                 "192.168.1.1");
    test_util_handle_events();

    // EXPECT for ipv4, devices will be created in gmap
    s_expect_dev_create("00:00:00:00:00:05", "id5", AF_INET, false);
    s_expect_dev_create("00:00:00:00:00:06", "id6", AF_INET, true);

    // EXPECT for ipv6, all will be ignored

    // EXPECT IPv4 that exists will not be touched
    amxc_var_new(&ip_found);
    amxc_var_set_type(ip_found, AMXC_VAR_ID_HTABLE);
    mock_gmap_expect_ip_device_get_address("id5", AF_INET, "192.168.1.5", ip_found);

    // EXPECT that the IPv4s that does not exist in gmap, will be verified
    //        (and not just added immediately because that would skip collision check)
    // id6:
    mock_gmap_expect_ip_device_get_address("id6", AF_INET, "192.168.1.6", NULL);
    mock_discoping_expect_verify("192.168.1.6", "00:00:00:00:00:06");

    // EXPECT error, incomplete, unreachable IPs will not be created
    // (nothing to do to expect that)

    // WHEN new items are added to the bridgetable
    test_util_parse_odl("netdev-mockdata-neigh.odl");
    s_emit_all_created();
    test_util_handle_events();
    s_clean_neighbors(&neighbor);
}

void test_gmap_eth_dev_neighbors__notification_new_entries_ipv6(void** state) {
    neighbors_test_instance_t* neighbor = NULL;

    // GIVEN a gmap-mod-ethernet-dev and a netdev with empty bridgetables
    test_util_parse_odl("netdev-dm.odl");
    test_util_parse_odl("netdev-mockdata-bridges.odl");
    test_setup_component_init(state);
    neighbor = s_setup_neighbors("bridge1", 2,
                                 "60:50:40:30:20:10",
                                 AF_INET6,
                                 "fe80::5877:88ff:fe48:3702");
    test_util_handle_events();

    // EXPECT for ipv4, all will be ignored

    // EXPECT for ipv6, discoping will be told netdev saw a neighbor advertisement
    mock_discoping_expect_mark_seen_network("fe80::6e48:ac89:9e00:4303", "00:00:00:00:00:03");
    mock_discoping_expect_mark_seen_network("2a02:1802:94:4200:211:11ff:fe01:2410", "00:00:00:00:00:10");

    // EXPECT for ipv6, discoping will not periodically verify since we rely on NetDev
    //        for that as long as NetDev says it's reachable
    mock_discoping_expect_set_periodic_reverify_enabled("fe80::6e48:ac89:9e00:4303", false);
    mock_discoping_expect_set_periodic_reverify_enabled("2a02:1802:94:4200:211:11ff:fe01:2410", false);

    // EXPECT error, incomplete, unreachable IPs will not be created
    // (nothing to do to expect that)

    // WHEN new items are added to the bridgetable
    test_util_parse_odl("netdev-mockdata-neigh.odl");
    s_emit_all_created();
    test_util_handle_events();
    s_clean_neighbors(&neighbor);
}

void test_gmap_eth_dev_neighbors__notification_new_entries_other_bridge(void** state) {
    neighbors_test_instance_t* neighbor = NULL;

    // GIVEN a gmap-mod-ethernet-dev and a netdev with empty bridgetables
    test_util_parse_odl("netdev-dm.odl");
    test_util_parse_odl("netdev-mockdata-bridges.odl");
    test_setup_component_init(state);
    neighbor = s_setup_neighbors("bridge3", 3,
                                 "60:50:40:30:20:10",
                                 AF_INET,
                                 "192.168.1.2");
    test_util_handle_events();

    // EXPECT for ipv4, devices will be created in gmap
    s_expect_dev_create("00:00:00:00:00:07", "id7", AF_INET, false);

    // EXPECT for ipv6, all will be ignored

    // EXPECT that the IPv4s that does not exist in gmap, will be verified
    //        (and not just added immediately because that would skip collision check)
    // id7:
    mock_gmap_expect_ip_device_get_address("id7", AF_INET, "169.254.1.7", NULL);
    mock_discoping_expect_verify("169.254.1.7", "00:00:00:00:00:07");

    // EXPECT error, incomplete, unreachable IPs will not be created
    // (nothing to do to expect that)

    // WHEN new items are added to the bridgetable
    test_util_parse_odl("netdev-mockdata-neigh.odl");
    s_emit_all_created();
    test_util_handle_events();
    s_clean_neighbors(&neighbor);
}

static void s_copy_param_to_variant(amxc_var_t* out, const char* out_name, amxd_object_t* object, const char* name) {
    char* value = amxd_object_get_cstring_t(object, name, NULL);
    amxc_var_add_key(cstring_t, out, out_name, value);
    free(value);
}


static void s_set_state(const char* ip_state) {
    amxd_object_t* object = NULL;
    amxd_trans_t trans;
    amxc_var_t event_data;
    amxc_var_t* neigh_data = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&event_data);

    object = amxd_dm_findf(get_dummy_dm(), "NetDev.Link.bridge1.Neigh.myip");

    amxc_var_set_type(&event_data, AMXC_VAR_ID_HTABLE);
    neigh_data = amxc_var_add_new_key(&event_data, "NeighborData");
    amxc_var_set_type(neigh_data, AMXC_VAR_ID_HTABLE);
    s_copy_param_to_variant(neigh_data, "Family", object, "Family");
    s_copy_param_to_variant(neigh_data, "Flags", object, "Flags");
    s_copy_param_to_variant(neigh_data, "Dst", object, "Dst");
    s_copy_param_to_variant(neigh_data, "LLAddress", object, "LLAddress");
    amxc_var_add_key(cstring_t, neigh_data, "State", ip_state);
    s_copy_param_to_variant(neigh_data, "OldState", object, "State");

    if((strcmp(ip_state, "probe") != 0) &&
       (strcmp(ip_state, "delay") != 0) &&
       (strcmp(ip_state, "incomplete") != 0)) {
        amxd_object_emit_signal(object, "Neighbor!", &event_data);
    }
    amxc_var_clean(&event_data);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_cstring_t(&trans, "State", ip_state);
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);
    test_util_handle_events();
}

static neighbors_test_instance_t* s_init_with_ipv4(void** test_state, const char* ip_state) {
    neighbors_test_instance_t* neighbor = NULL;
    test_util_parse_odl("netdev-dm.odl");
    test_util_parse_odl("netdev-mockdata-bridges.odl");
    test_util_parse_odl("netdev-mockdata-neigh-reachable.odl");
    test_util_handle_events();
    s_set_state(ip_state);
    test_util_handle_events();

    s_expect_dev_create("00:00:00:00:01:23", "myid", AF_INET, false);
    mock_gmap_expect_ip_device_get_address("myid", AF_INET, "192.168.1.123", NULL);
    mock_discoping_expect_verify("192.168.1.123", "00:00:00:00:01:23");

    test_setup_component_init(test_state);
    neighbor = s_setup_neighbors("bridge1", 2,
                                 "60:50:40:30:20:10",
                                 AF_INET,
                                 "192.168.1.1");
    test_util_handle_events();
    return neighbor;
}

static void s_testhelper_ipv4_notification_become(void** test_state, const char* old_ip_state, const char* new_ip_state, bool expect_verify) {
    // GIVEN a reachable ip netdev's neighbor table with state `old_ip_state`
    neighbors_test_instance_t* neighbor = s_init_with_ipv4(test_state, old_ip_state);

    if(expect_verify) {
        // EXPECT device and ip to be verified (including collision check):
        s_expect_dev_create("00:00:00:00:01:23", "myid", AF_INET, false);
        mock_gmap_expect_ip_device_get_address("myid", AF_INET, "192.168.1.123", NULL);
        mock_discoping_expect_verify("192.168.1.123", "00:00:00:00:01:23");
    } else {
        // EXPECT no ip will be verified
    }

    // WHEN a neighbor entry in NetDev changes state to `new_ip_state`
    s_set_state(new_ip_state);

    s_clean_neighbors(&neighbor);
}

void test_gmap_eth_dev_neighbors__notification_ipv4_change_reachable_to_stale(void** test_state) {
    s_testhelper_ipv4_notification_become(test_state, "reachable", "stale", false);
}

void test_gmap_eth_dev_neighbors__notification_ipv4_change_stale_to_reachable(void** test_state) {
    s_testhelper_ipv4_notification_become(test_state, "stale", "reachable", true);
}

void test_gmap_eth_dev_neighbors__notification_ipv4_change_reachable_to_error(void** test_state) {
    s_testhelper_ipv4_notification_become(test_state, "reachable", "error", false);
}

void test_gmap_eth_dev_neighbors__notification_ipv4_change_error_to_reachable(void** test_state) {
    s_testhelper_ipv4_notification_become(test_state, "error", "reachable", true);
}

void test_gmap_eth_dev_neighbors__notification_ipv4_change_probe_to_reachable(void** test_state) {
    s_testhelper_ipv4_notification_become(test_state, "probe", "reachable", true);
}

void test_gmap_eth_dev_neighbors__notification_ipv4_change_failed_to_reachable(void** test_state) {
    s_testhelper_ipv4_notification_become(test_state, "failed", "reachable", true);
}

void test_gmap_eth_dev_neighbors__notification_ipv4_change_stale_to_error(void** test_state) {
    s_testhelper_ipv4_notification_become(test_state, "stale", "error", false);
}

void test_gmap_eth_dev_neighbors__notification_ipv4_change_error_to_stale(void** test_state) {
    s_testhelper_ipv4_notification_become(test_state, "error", "stale", false);
}

void test_gmap_eth_dev_neighbors__notification_ipv4_change_nonsense(void** test_state) {
    s_testhelper_ipv4_notification_become(test_state, "nonsense", "more nonsense", false);
}

static neighbors_test_instance_t* s_init_with_ipv6(void** test_state, const char* ip_state) {
    neighbors_test_instance_t* neighbor = NULL;
    test_util_parse_odl("netdev-dm.odl");
    test_util_parse_odl("netdev-mockdata-bridges.odl");
    test_util_parse_odl("netdev-mockdata-neigh-reachable-ipv6.odl");
    test_util_handle_events();
    s_set_state(ip_state);
    test_util_handle_events();

    s_expect_dev_create("00:00:00:00:01:23", "myid", AF_INET6, false);
    mock_discoping_expect_verify_once("fe80::1:2:3:123", "00:00:00:00:01:23");

    test_setup_component_init(test_state);
    neighbor = s_setup_neighbors("bridge1", 2,
                                 "60:50:40:30:20:10",
                                 AF_INET6,
                                 "fe80::5877:88ff:fe48:3702");
    test_util_handle_events();
    return neighbor;
}

static void s_testhelper_ipv6_neighbor_advertisement_seen(void** test_state, const char* old_state, const char* new_state) {
    // GIVEN
    neighbors_test_instance_t* neighbor = s_init_with_ipv6(test_state, old_state);

    // EXPECT
    mock_discoping_expect_mark_seen_network("fe80::1:2:3:123", "00:00:00:00:01:23");
    mock_discoping_expect_set_periodic_reverify_enabled("fe80::1:2:3:123", false);

    // WHEN
    s_set_state(new_state);

    s_clean_neighbors(&neighbor);
}

static void s_testhelper_ipv6_neighbor_advertisement_timeout(void** test_state, const char* old_state, const char* new_state) {
    // GIVEN
    neighbors_test_instance_t* neighbor = s_init_with_ipv6(test_state, old_state);

    // EXPECT
    mock_discoping_expect_set_periodic_reverify_enabled("fe80::1:2:3:123", true);

    // WHEN
    s_set_state(new_state);

    s_clean_neighbors(&neighbor);
}

void test_gmap_eth_dev_neighbors__notification_ipv6_change_reachable_to_stale(void** test_state) {
    s_testhelper_ipv6_neighbor_advertisement_timeout(test_state, "reachable", "stale");

}

void test_gmap_eth_dev_neighbors__notification_ipv6_change_stale_to_reachable(void** test_state) {
    s_testhelper_ipv6_neighbor_advertisement_seen(test_state, "stale", "reachable");
}

void test_gmap_eth_dev_neighbors__notification_ipv6_change_reachable_to_error(void** test_state) {
    s_testhelper_ipv6_neighbor_advertisement_timeout(test_state, "reachable", "error");
}

void test_gmap_eth_dev_neighbors__notification_ipv6_change_error_to_reachable(void** test_state) {
    s_testhelper_ipv6_neighbor_advertisement_seen(test_state, "error", "reachable");
}

void test_gmap_eth_dev_neighbors__notification_ipv6_change_probe_to_reachable(void** test_state) {
    s_testhelper_ipv6_neighbor_advertisement_seen(test_state, "probe", "reachable");
}

void test_gmap_eth_dev_neighbors__notification_ipv6_change_failed_to_reachable(void** test_state) {
    s_testhelper_ipv6_neighbor_advertisement_seen(test_state, "failed", "reachable");
}

void test_gmap_eth_dev_neighbors__notification_ipv6_change_stale_to_error(void** test_state) {
    // GIVEN
    neighbors_test_instance_t* neighbor = s_init_with_ipv6(test_state, "stale");

    // EXPECTED nothing

    // WHEN
    s_set_state("error");

    s_clean_neighbors(&neighbor);
}

void test_gmap_eth_dev_neighbors__notification_ipv6_change_error_to_stale(void** test_state) {
    // GIVEN
    neighbors_test_instance_t* neighbor = s_init_with_ipv6(test_state, "error");

    // EXPECTED nothing

    // WHEN
    s_set_state("stale");

    s_clean_neighbors(&neighbor);
}

void test_gmap_eth_dev_neighbors__notification_ipv6_change_nonsense(void** test_state) {
    // GIVEN
    neighbors_test_instance_t* neighbor = s_init_with_ipv6(test_state, "nonsense");

    // EXPECTED nothing

    // WHEN
    s_set_state("more nonsense");

    s_clean_neighbors(&neighbor);
}
