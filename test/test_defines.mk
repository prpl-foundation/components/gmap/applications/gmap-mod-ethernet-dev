MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
# for OBJDIR no `$(realpath something)` since it does not always exist yet initially.
OBJDIR = ../../output/$(MACHINE)/coverage
INCDIR = $(realpath ../../include ../../include_priv)
MOCK_SRCDIR = $(realpath ../common/)

HEADERS = $(wildcard $(INCDIR)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c)
SOURCES += $(wildcard $(MOCK_SRCDIR)/*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxc -lamxj -lamxp -lamxd -lamxo -lamxb \
		   -lgmap-client -lsahtrace -ldhcpoptions -ldiscoping -lgmap-ext \
		   -ldl -lpthread

WRAP_FUNC=-Wl,--wrap=

MOCK_WRAP+= gmap_devices_linkAdd \
            gmap_devices_linkReplace \
            gmap_devices_linkRemove \
            gmap_devices_createDevice \
            gmap_device_get \
            gmap_device_set \
            gmap_devices_createDevice_ext \
            gmap_devices_createDeviceOrGetKey \
            gmap_devices_createIfActive \
            gmap_device_setTag \
            gmap_device_setName \
            gmap_ip_device_delete_address \
            gmap_ip_device_add_address \
            gmap_ip_device_get_address \
            gmap_ip_device_set_address \
            gmap_devices_find \
            gmap_devices_findByMac \
            gmap_devices_findByIp \
            gmap_query_open_ext \
            gmap_query_open_ext_2 \
            gmap_query_close \
            gmap_device_setActive \
            gmap_device_clearTag \
            gmap_device_setName \
            gmap_config_get \
            amxb_be_who_has \
            amxb_wait_for_object \
            discoping_new \
            discoping_delete \
            discoping_open \
            discoping_close \
            discoping_set_on_up_cb \
            discoping_set_on_down_cb \
            discoping_set_on_collision_cb \
            discoping_set_userdata \
            discoping_verify \
            discoping_verify_once \
            discoping_mark_seen_network \
            discoping_set_periodic_reverify_enabled \
            discoping_get_open_sockets \
            netmodel_initialize \
            netmodel_cleanup \
            netmodel_openQuery_getFirstParameter \
            netmodel_closeQuery \

LDFLAGS += -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))
