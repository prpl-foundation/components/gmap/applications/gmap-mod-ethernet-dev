/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include <fcntl.h>
#include "gmap_eth_dev.h"
#include "gmap_eth_dev_discoping.h"
#include <yajl/yajl_gen.h> // required by amxj/amxj_variant.h
#include "amxj/amxj_variant.h"
#include "amxd/amxd_object_event.h"
#include "amxo/amxo.h"
#include "amxd/amxd_object.h"
#include <string.h>
#include <amxd/amxd_transaction.h>
#include <arpa/inet.h>
#include <gmap/extensions/ip/gmap_ext_ipaddress.h>

#include "../common/test-setup.h"
#include "../common/test-util.h"
#include "../common/mock_gmap.h"
#include "test_bridge.h"
#include "../common/mock_discoping.h"

static void s_dump_datamodel(const char* expression) {
    amxb_bus_ctx_t* bus = get_dummy_bus_ctx();
    int status = -1;
    amxc_var_t res;

    amxc_var_init(&res);
    printf("Dumping data model '%s' from 0x%lx:", expression, (uintptr_t) bus);
    fflush(stdout);
    status = amxb_get(bus, expression, 100, &res, 100);
    printf("%d\n", status);
    fflush(stdout);

    amxc_var_dump(&res, STDOUT_FILENO);

    amxc_var_clean(&res);
}

/** construct result that gmap_device_get will return */
static amxc_var_t* s_retval_of_gmap_get(const char* key, const char* netdev_name, uint32_t netdev_index, const char* mac) {
    amxc_var_t* dev;
    amxc_var_new(&dev);
    amxc_var_set_type(dev, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, dev, "Key", key);
    amxc_var_add_key(cstring_t, dev, "PhysAddress", mac);
    amxc_var_add_key(cstring_t, dev, "NetDevName", netdev_name);
    amxc_var_add_key(uint32_t, dev, "NetDevIndex", netdev_index);
    return dev;
}

static void s_set_ip_data(gmap_query_t* query_macip, const char* mac, gmap_query_action_t action) {
    amxc_var_t bridge_query_fields;
    amxc_var_t* gmap_bridge = s_retval_of_gmap_get("bridge1", "bridge_netdevname", 1, mac);

    amxc_var_init(&bridge_query_fields);
    amxc_var_set_type(&bridge_query_fields, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &bridge_query_fields, "Key", "bridge1");
    amxc_var_add_key(cstring_t, &bridge_query_fields, "PhysAddress", mac);
    amxc_var_add_key(cstring_t, &bridge_query_fields, "NetDevName", "bridge_netdevname");
    amxc_var_add_key(uint32_t, &bridge_query_fields, "NetDevIndex", 1);

    query_macip->fn(query_macip, "bridge1", &bridge_query_fields, action);

    amxc_var_clean(&bridge_query_fields);
    amxc_var_delete(&gmap_bridge);
}

static void s_remove_ip_data(gmap_query_t* query_macip, const char* mac) {
    amxc_var_t bridge_query_fields;
    amxc_var_init(&bridge_query_fields);
    amxc_var_set_type(&bridge_query_fields, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &bridge_query_fields, "Key", "bridge1");
    amxc_var_add_key(cstring_t, &bridge_query_fields, "PhysAddress", mac);
    amxc_var_add_key(cstring_t, &bridge_query_fields, "NetDevName", "bridge_netdevname");
    amxc_var_add_key(uint32_t, &bridge_query_fields, "NetDevIndex", 1);

    query_macip->fn(query_macip, "bridge1", &bridge_query_fields, gmap_query_expression_stop_matching);

    test_util_handle_events();
    amxc_var_clean(&bridge_query_fields);
}

static void s_create_bridge_and_discoping_ext(
    mock_discoping_t* mock_ext_discoping,
    gmap_eth_dev_bridge_collection_t** bridges,
    gmap_query_t* query_macip,
    bool has_mac) {

    assert_non_null(mock_ext_discoping);
    assert_non_null(bridges);
    assert_null(*bridges);
    assert_non_null(query_macip);

    gmap_client_init(get_dummy_bus_ctx());

    expect_string(__wrap_gmap_query_open_ext_2, expression, "bridge mac interface lan && .PhysAddress != '' && .NetDevName != '' && (ipv4 || ipv6)");
    will_return(__wrap_gmap_query_open_ext_2, query_macip);

    gmap_eth_dev_bridge_collection_new(bridges, get_dummy_parser());
    assert_non_null(*bridges);

    test_util_parse_odl("../common/dummy-gmap.odl");
    test_util_parse_odl("../common/gmap-mockdata-bridge.odl");
    if(has_mac) {
        test_util_parse_odl("../common/gmap-mockdata-bridge-mac.odl");
    }
    test_util_handle_events();
}

static void s_expect_bridge_modules(uint32_t protocol) {
    will_return(__wrap_gmap_eth_dev_bridgetable_new, NULL);
    if((protocol == AF_INET) && (gmap_eth_dev_get_ctx_dhcp() != NULL)) {
        will_return(__wrap_gmap_eth_dev_dhcp_reader_new, NULL);
    }
    will_return(__wrap_gmap_eth_dev_neighbors_new, NULL);
}

static void s_test_gmap_eth_dev_bridge__bridge_new_ip(const char* odl_file, const char* ip, uint32_t protocol) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_eth_dev_bridge_t* bridge = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping = {0};

    // GIVEN an discoping and a gmap bridge without IP
    s_create_bridge_and_discoping_ext(&mock_ext_discoping, &bridges, &query_macip, true);

    // WHEN the bridge receives a new IP
    test_util_parse_odl(odl_file);
    test_util_handle_events();

    will_return(__wrap_discoping_new, &mock_ext_discoping);
    s_expect_bridge_modules(protocol);
    s_set_ip_data(&query_macip, "0b:07:01:0d:09:03", gmap_query_expression_start_matching);
    test_util_handle_events();

    // THEN it has a bridge
    for(amxc_htable_it_t* it = amxc_htable_get(&bridges->bridges, "bridge1");
        it != NULL;
        it = amxc_htable_it_get_next_key(it)) {
        gmap_eth_dev_bridge_t* it_bridge = amxc_htable_it_get_data(it, gmap_eth_dev_bridge_t, it);
        if(it_bridge->proto_family == protocol) {
            assert_null(bridge);
            bridge = it_bridge;
        }
    }
    assert_non_null(bridge);
    assert_string_equal(gmap_eth_dev_bridge_get_ip_address((bridge)), ip);

    // AND it started listening on incoming ARP messages
    mock_discoping_assert(&mock_ext_discoping, 1, 0, "bridge_netdevname", "0b:07:01:0d:09:03", ip);

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
}

void test_gmap_eth_dev_bridge__bridge_new_ip(UNUSED void** state) {
    s_test_gmap_eth_dev_bridge__bridge_new_ip("gmap-mockdata-bridge-ip.odl", "192.168.1.1", AF_INET);
}

void test_gmap_eth_dev_bridge__bridge_new_ipv6(UNUSED void** state) {
    s_test_gmap_eth_dev_bridge__bridge_new_ip("gmap-mockdata-bridge-ipv6.odl", "2a02:1802:94:43c0::1", AF_INET6);
}

static void s_test_gmap_eth_dev_bridge__bridge_already_had_ip(const char* odl, const char* ip, uint32_t protocol) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping = {0};
    amxc_var_t* gmap_bridge = s_retval_of_gmap_get("bridge1", "bridge_netdevname", 1, "0b:07:01:0d:09:03");

    // GIVEN a gmap bridge with IP
    gmap_client_init(get_dummy_bus_ctx());
    test_util_parse_odl("../common/dummy-gmap.odl");
    test_util_parse_odl("../common/gmap-mockdata-bridge.odl");
    test_util_parse_odl("../common/gmap-mockdata-bridge-mac.odl");
    test_util_parse_odl(odl);
    test_util_handle_events();

    // WHEN starting discoping
    gmap_client_init(get_dummy_bus_ctx());

    expect_string(__wrap_gmap_query_open_ext_2, expression, "bridge mac interface lan && .PhysAddress != '' && .NetDevName != '' && (ipv4 || ipv6)");
    will_return(__wrap_gmap_query_open_ext_2, &query_macip);

    gmap_eth_dev_bridge_collection_new(&bridges, get_dummy_parser());
    assert_non_null(bridges);
    will_return(__wrap_discoping_new, &mock_ext_discoping);
    s_expect_bridge_modules(protocol);
    s_set_ip_data(&query_macip, "0b:07:01:0d:09:03", gmap_query_expression_start_matching);

    // THEN it started listening on incoming ARP messages
    mock_discoping_assert(&mock_ext_discoping, 1, 0, "bridge_netdevname", "0b:07:01:0d:09:03", ip);

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
    amxc_var_delete(&gmap_bridge);
}

void test_gmap_eth_dev_bridge__bridge_already_had_ipv4(UNUSED void** state) {
    s_test_gmap_eth_dev_bridge__bridge_already_had_ip("gmap-mockdata-bridge-ip.odl", "192.168.1.1", AF_INET);
}

void test_gmap_eth_dev_bridge__bridge_already_had_ipv6(UNUSED void** state) {
    s_test_gmap_eth_dev_bridge__bridge_already_had_ip("gmap-mockdata-bridge-ipv6.odl", "2a02:1802:94:43c0::1", AF_INET6);
}

static void s_test_gmap_eth_dev_bridge__bridge_new_ip_no_mac(const char* odl) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping = {0};
    amxc_var_t* gmap_bridge = NULL;

    // GIVEN an discoping and a gmap bridge without IP and without mac
    s_create_bridge_and_discoping_ext(&mock_ext_discoping, &bridges, &query_macip, false);

    // WHEN the bridge receives a new IP
    test_util_parse_odl(odl);
    test_util_handle_events();

    s_set_ip_data(&query_macip, NULL, gmap_query_expression_start_matching);
    test_util_handle_events();

    // THEN it does not start listening on incoming ARP messages yet
    // don't return(__wrap_discoping_new, &mock_ext_discoping);
    mock_discoping_assert(&mock_ext_discoping, 0, 0, NULL, NULL, NULL);

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
    amxc_var_delete(&gmap_bridge);
}

void test_gmap_eth_dev_bridge__bridge_new_ipv4_no_mac(UNUSED void** state) {
    s_test_gmap_eth_dev_bridge__bridge_new_ip_no_mac("gmap-mockdata-bridge-ip.odl");
}

void test_gmap_eth_dev_bridge__bridge_new_ipv6_no_mac(UNUSED void** state) {
    s_test_gmap_eth_dev_bridge__bridge_new_ip_no_mac("gmap-mockdata-bridge-ipv6.odl");
}

static void s_set_mac_data(amxc_var_t* bridge_query_fields, amxc_var_t* gmap_bridge, const char* odl, uint32_t netdev_index, const char* mac) {
    amxc_var_t* old_mac_var = NULL;
    test_util_parse_odl(odl);
    amxc_var_set_type(bridge_query_fields, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, bridge_query_fields, "Key", "bridge1");
    amxc_var_add_key(cstring_t, bridge_query_fields, "PhysAddress", mac);
    amxc_var_add_key(cstring_t, bridge_query_fields, "NetDevName", "bridge_netdevname");
    amxc_var_add_key(uint32_t, bridge_query_fields, "NetDevIndex", netdev_index);
    if(gmap_bridge != NULL) {
        old_mac_var = amxc_var_take_key(gmap_bridge, "PhysAddress");
        amxc_var_delete(&old_mac_var);
        amxc_var_add_key(cstring_t, gmap_bridge, "PhysAddress", mac);
        assert_string_equal(GET_CHAR(gmap_bridge, "PhysAddress"), mac);
    }
}

/** this is for when the bridge did not have a mac before. */
static void s_bridge_receives_first_new_mac(gmap_query_t* query_macip, amxc_var_t* gmap_bridge) {
    amxc_var_t bridge_query_fields;
    amxc_var_init(&bridge_query_fields);
    s_set_mac_data(&bridge_query_fields, gmap_bridge, "../common/gmap-mockdata-bridge-mac.odl", 1, "0b:07:01:0d:09:03");
    query_macip->fn(query_macip, "bridge1", &bridge_query_fields, gmap_query_expression_start_matching);
    test_util_handle_events();
    amxc_var_clean(&bridge_query_fields);
}

static void s_bridge_changes_mac(gmap_query_t* query_macip, amxc_var_t* gmap_bridge, const char* odl, uint32_t netdev_index, const char* new_mac) {
    amxc_var_t bridge_query_fields;
    amxc_var_init(&bridge_query_fields);
    s_set_mac_data(&bridge_query_fields, gmap_bridge, odl, netdev_index, new_mac);
    query_macip->fn(query_macip, "bridge1", &bridge_query_fields, gmap_query_device_updated);
    test_util_handle_events();
    amxc_var_clean(&bridge_query_fields);
}

void test_gmap_eth_dev_bridge__bridge_new_mac_no_ip(UNUSED void** state) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping = {0};

    // GIVEN an discoping and a gmap bridge without IP and without mac
    s_create_bridge_and_discoping_ext(&mock_ext_discoping, &bridges, &query_macip, false);

    // WHEN the bridge receives a new MAC
    s_dump_datamodel("Devices.");
    s_bridge_receives_first_new_mac(&query_macip, NULL);

    // THEN it does not start listening on incoming ARP messages yet
    // don't return(__wrap_discoping_new, &mock_ext_discoping);
    mock_discoping_assert(&mock_ext_discoping, 0, 0, NULL, NULL, NULL);

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
}

static void s_test_gmap_eth_dev_bridge__bridge_new_mac_already_has_ip(const char* odl, const char* ip, uint32_t protocol) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping = {0};
    amxc_var_t* gmap_bridge = NULL;

    // GIVEN an discoping and a gmap bridge with IP and without mac
    s_create_bridge_and_discoping_ext(&mock_ext_discoping, &bridges, &query_macip, false);

    test_util_parse_odl(odl);
    test_util_handle_events();

    s_set_ip_data(&query_macip, NULL, gmap_query_expression_start_matching);
    test_util_handle_events();
    // WHEN the bridge receives a new MAC
    will_return(__wrap_discoping_new, &mock_ext_discoping);
    s_expect_bridge_modules(protocol);
    s_bridge_receives_first_new_mac(&query_macip, gmap_bridge);

    // THEN it started listening on incoming ARP/NDP messages
    mock_discoping_assert(&mock_ext_discoping, 1, 0, "bridge_netdevname", "0b:07:01:0d:09:03", ip);

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
    amxc_var_delete(&gmap_bridge);
}

void test_gmap_eth_dev_bridge__bridge_new_mac_already_has_ipv4(UNUSED void** state) {
    s_test_gmap_eth_dev_bridge__bridge_new_mac_already_has_ip("gmap-mockdata-bridge-ip.odl", "192.168.1.1", AF_INET);
}

void test_gmap_eth_dev_bridge__bridge_new_mac_already_has_ipv6(UNUSED void** state) {
    s_test_gmap_eth_dev_bridge__bridge_new_mac_already_has_ip("gmap-mockdata-bridge-ipv6.odl", "2a02:1802:94:43c0::1", AF_INET6);
}

static void s_create_bridge_and_discoping(
    mock_discoping_t* mock_ext_discoping,
    amxc_var_t** gmap_bridge,
    gmap_eth_dev_bridge_collection_t** bridges,
    gmap_query_t* query_macip,
    const char* ip_odl,
    const char* ip,
    uint32_t protocol) {

    assert_non_null(mock_ext_discoping);
    assert_non_null(gmap_bridge);
    assert_non_null(bridges);
    assert_null(*bridges);
    assert_non_null(query_macip);

    gmap_client_init(get_dummy_bus_ctx());

    *gmap_bridge = s_retval_of_gmap_get("bridge1", "bridge_netdevname", 1, "0b:07:01:0d:09:03");

    expect_string(__wrap_gmap_query_open_ext_2, expression, "bridge mac interface lan && .PhysAddress != '' && .NetDevName != '' && (ipv4 || ipv6)");
    will_return(__wrap_gmap_query_open_ext_2, query_macip);
    will_return(__wrap_discoping_new, mock_ext_discoping);
    s_expect_bridge_modules(protocol);
    gmap_eth_dev_bridge_collection_new(bridges, get_dummy_parser());
    assert_non_null(*bridges);

    test_util_parse_odl("../common/dummy-gmap.odl");
    test_util_parse_odl("../common/gmap-mockdata-bridge.odl");
    test_util_parse_odl("../common/gmap-mockdata-bridge-mac.odl");
    test_util_parse_odl(ip_odl);
    test_util_handle_events();

    s_set_ip_data(query_macip, "0b:07:01:0d:09:03", gmap_query_expression_start_matching);
    test_util_handle_events();

    mock_discoping_assert(mock_ext_discoping, 1, 0, "bridge_netdevname", "0b:07:01:0d:09:03", ip);
}

static void s_test_gmap_eth_dev_bridge__bridge_ip_removed(const char* ip_odl, const char* ip, UNUSED uint32_t protocol, const char* path) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping = {0};
    amxc_var_t* gmap_bridge = NULL;
    amxd_trans_t trans;

    // GIVEN an discoping with opened "arp socket":
    s_create_bridge_and_discoping(&mock_ext_discoping, &gmap_bridge, &bridges, &query_macip, ip_odl, ip, protocol);

    // WHEN the ip of the bridge is removed
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "%s", path);
    amxd_trans_del_inst(&trans, 1, NULL);
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);

    test_util_handle_events();

    s_remove_ip_data(&query_macip, "0b:07:01:0d:09:03");

    // THEN it stopped listening on incoming ARP/NDP messages
    mock_discoping_assert(&mock_ext_discoping, 1, 1, NULL, NULL, NULL);

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
    amxc_var_delete(&gmap_bridge);
}

void test_gmap_eth_dev_bridge__bridge_ipv4_removed(UNUSED void** state) {
    s_test_gmap_eth_dev_bridge__bridge_ip_removed("gmap-mockdata-bridge-ip.odl", "192.168.1.1", AF_INET, "Devices.Device.bridge1.IPv4Address.");
}

void test_gmap_eth_dev_bridge__bridge_ipv6_removed(UNUSED void** state) {
    s_test_gmap_eth_dev_bridge__bridge_ip_removed("gmap-mockdata-bridge-ipv6.odl", "2a02:1802:94:43c0::1", AF_INET6, "Devices.Device.bridge1.IPv6Address.");
}

static void s_test_gmap_eth_dev_bridge__bridge_ip_changes(const char* odl_ip, const char* odl_ip2, const char* ip_old, const char* new_ip, uint32_t protocol, UNUSED const char* path) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping1 = { .keep_log = true };
    mock_discoping_t mock_ext_discoping2 = { .keep_log = true };
    amxc_var_t* gmap_bridge = NULL;

    // GIVEN an discoping with opened "arp socket":
    s_create_bridge_and_discoping(&mock_ext_discoping1, &gmap_bridge, &bridges, &query_macip, odl_ip, ip_old, protocol);

    // WHEN the IP changes
    test_util_parse_odl(odl_ip2);
    will_return(__wrap_discoping_new, &mock_ext_discoping2);
    s_expect_bridge_modules(protocol);  // for bridge isntance recreation
    s_set_ip_data(&query_macip, "0b:07:01:0d:09:03", gmap_query_device_updated);
    test_util_handle_events();

    // THEN it closed and opened the ARP socket, with the new IP
    mock_discoping_assert_log(&mock_ext_discoping1, 0, "open", "0b:07:01:0d:09:03", ip_old, "bridge_netdevname");
    mock_discoping_assert_log(&mock_ext_discoping1, 1, "close", "0b:07:01:0d:09:03", ip_old, "bridge_netdevname");
    mock_discoping_assert_log(&mock_ext_discoping2, 0, "open", "0b:07:01:0d:09:03", new_ip, "bridge_netdevname");

    assert_int_equal(2, amxc_llist_size(amxc_var_constcast(amxc_llist_t, &mock_ext_discoping1.log)));
    assert_int_equal(1, amxc_llist_size(amxc_var_constcast(amxc_llist_t, &mock_ext_discoping2.log)));

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
    amxc_var_delete(&gmap_bridge);
    amxc_var_clean(&mock_ext_discoping1.log);
    amxc_var_clean(&mock_ext_discoping2.log);
}

void test_gmap_eth_dev_bridge__bridge_ipv4_changes(UNUSED void** state) {
    s_test_gmap_eth_dev_bridge__bridge_ip_changes("gmap-mockdata-bridge-ip.odl", "gmap-mockdata-bridge-ip2.odl", "192.168.1.1", "192.168.1.2", AF_INET, "Devices.Device.bridge1.IPv4Address.1.");
}

void test_gmap_eth_dev_bridge__bridge_ipv6_changes(UNUSED void** state) {
    s_test_gmap_eth_dev_bridge__bridge_ip_changes("gmap-mockdata-bridge-ipv6.odl", "gmap-mockdata-bridge-ipv6_2.odl", "2a02:1802:94:43c0::1", "2a02:1802:94:43c0::2", AF_INET6, "Devices.Device.bridge1.IPv6Address.1.");
}

static void s_test_gmap_eth_dev_bridge__bridge_late_dhcp(const char* odl_file, uint32_t protocol) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping = {0};

    // GIVEN an discoping and a gmap bridge without IP
    s_create_bridge_and_discoping_ext(&mock_ext_discoping, &bridges, &query_macip, true);

    // WHEN the bridge receives a new IP
    test_util_parse_odl(odl_file);
    test_util_handle_events();

    will_return(__wrap_discoping_new, &mock_ext_discoping);
    s_expect_bridge_modules(protocol);
    s_set_ip_data(&query_macip, "0b:07:01:0d:09:03", gmap_query_expression_start_matching);
    test_util_handle_events();

    // THEN it has a bridge
    assert_int_equal(amxc_htable_size(&bridges->bridges), 1);

    // WHEN the DHCPv4 server becomes available
    // THEN a dhcp reader is instantiated for the ipv4 bridge
    assert_null(gmap_eth_dev_get_ctx_dhcp());
    if(protocol == AF_INET) {
        will_return(__wrap_gmap_eth_dev_dhcp_reader_new, NULL);
    }
    gmap_eth_dev_bridge_collection_dhcpserver_available(bridges, get_dummy_bus_ctx());

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
}

void test_gmap_eth_dev_bridge__bridge_late_dhcp_ip(UNUSED void** state) {
    s_test_gmap_eth_dev_bridge__bridge_late_dhcp("gmap-mockdata-bridge-ip.odl", AF_INET);
}

void test_gmap_eth_dev_bridge__bridge_late_dhcp_ipv6(UNUSED void** state) {
    s_test_gmap_eth_dev_bridge__bridge_late_dhcp("gmap-mockdata-bridge-ipv6.odl", AF_INET6);
}

void test_gmap_eth_dev_bridge__bridge_changes_mac(UNUSED void** state) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping1 = { .keep_log = true };
    mock_discoping_t mock_ext_discoping2 = { .keep_log = true };
    amxc_var_t* gmap_bridge = NULL;

    // GIVEN an discoping with opened "arp socket":
    s_create_bridge_and_discoping(&mock_ext_discoping1, &gmap_bridge, &bridges, &query_macip, "gmap-mockdata-bridge-ip.odl", "192.168.1.1", AF_INET);

    // WHEN the bridge receives a new MAC
    will_return(__wrap_discoping_new, &mock_ext_discoping2);
    s_expect_bridge_modules(AF_INET); /* For the re-created bridge instance */
    s_bridge_changes_mac(&query_macip, gmap_bridge, "gmap-mockdata-bridge-mac2.odl", 1, "0b:07:01:0d:09:02");

    // THEN it closed the old socket on which it must not send anymore
    mock_discoping_assert_log(&mock_ext_discoping1, 1, "close", "0b:07:01:0d:09:03", "192.168.1.1", "bridge_netdevname");

    // AND THEN it opened the new socket
    mock_discoping_assert_log(&mock_ext_discoping2, 0, "open", "0b:07:01:0d:09:02", "192.168.1.1", "bridge_netdevname");

    // AND THEN it did nothing else
    assert_int_equal(2, amxc_llist_size(amxc_var_constcast(amxc_llist_t, &mock_ext_discoping1.log)));
    assert_int_equal(1, amxc_llist_size(amxc_var_constcast(amxc_llist_t, &mock_ext_discoping2.log)));

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
    amxc_var_delete(&gmap_bridge);
    amxc_var_clean(&mock_ext_discoping1.log);
    amxc_var_clean(&mock_ext_discoping2.log);
}

void test_gmap_eth_dev_bridge__bridge_notif_for_irrelevant_field(UNUSED void** state) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping = {0};
    amxc_var_t* gmap_bridge = NULL;

    // GIVEN an discoping and a gmap bridge with IP and mac
    s_create_bridge_and_discoping(&mock_ext_discoping, &gmap_bridge, &bridges, &query_macip, "gmap-mockdata-bridge-ip.odl", "192.168.1.1", AF_INET);

    // WHEN a bridge change happens for a irrelevant field (so the relevant fields stay the same)
    s_bridge_changes_mac(&query_macip, gmap_bridge, "../common/gmap-mockdata-bridge-mac.odl", 1, "0b:07:01:0d:09:03");

    // THEN nothing happens
    assert_int_equal(1, amxc_llist_size(amxc_var_constcast(amxc_llist_t, &mock_ext_discoping.log)));

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
    amxc_var_delete(&gmap_bridge);
}

static void s_test_mac_and_ip_change_at_same_time(bool ip_query_first) {
    const char* odl_ip = "gmap-mockdata-bridge-ip.odl";
    const char* odl_ip2 = "gmap-mockdata-bridge-ip2.odl";
    const char* ip_old = "192.168.1.1";
    const char* new_ip = "192.168.1.2";
    const char* mac_old = "0b:07:01:0d:09:03";
    const char* mac_new = "0b:07:01:0d:09:02";

    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping1 = { .keep_log = true };
    mock_discoping_t mock_ext_discoping2 = { .keep_log = true };
    amxc_var_t* gmap_bridge = NULL;
    amxc_var_t bridge_query_fields;

    // GIVEN an discoping with opened "arp socket":
    s_create_bridge_and_discoping(&mock_ext_discoping1, &gmap_bridge, &bridges, &query_macip, odl_ip, ip_old, AF_INET);

    s_bridge_receives_first_new_mac(&query_macip, gmap_bridge);
    test_util_handle_events();

    // WHEN the IP changes and the MAC changes at the same time
    test_util_parse_odl(odl_ip2);

    // new data of new mac:
    amxc_var_init(&bridge_query_fields);
    s_set_mac_data(&bridge_query_fields, gmap_bridge, "gmap-mockdata-bridge-mac2.odl", 1, mac_new);

    will_return(__wrap_discoping_new, &mock_ext_discoping2);
    s_expect_bridge_modules(AF_INET); /* When the bridge is recreated, but only once */
    if(ip_query_first) {
        // new data of new ip:
        s_set_ip_data(&query_macip, mac_new, gmap_query_device_updated);
        test_util_handle_events();
        query_macip.fn(&query_macip, "bridge1", &bridge_query_fields, gmap_query_device_updated);
    } else {
        query_macip.fn(&query_macip, "bridge1", &bridge_query_fields, gmap_query_device_updated);
        // new data of new ip:
        s_set_ip_data(&query_macip, mac_new, gmap_query_device_updated);
        test_util_handle_events();
    }

    // THEN it closed and opened the ARP socket, with the new IP and MAC
    mock_discoping_assert_log(&mock_ext_discoping1, 1,
                              "close",
                              mac_old,
                              ip_old,
                              "bridge_netdevname");
    mock_discoping_assert_log(&mock_ext_discoping2, 0,
                              "open",
                              mac_new,
                              new_ip,
                              "bridge_netdevname");

    assert_int_equal(2, amxc_llist_size(amxc_var_constcast(amxc_llist_t, &mock_ext_discoping1.log)));
    assert_int_equal(1, amxc_llist_size(amxc_var_constcast(amxc_llist_t, &mock_ext_discoping2.log)));

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
    amxc_var_delete(&gmap_bridge);
    amxc_var_clean(&bridge_query_fields);
    amxc_var_clean(&mock_ext_discoping1.log);
    amxc_var_clean(&mock_ext_discoping2.log);
}

void test_gmap_eth_dev_bridge__mac_query_while_ip_also_changed(UNUSED void** state) {
    s_test_mac_and_ip_change_at_same_time(false);
}

void test_gmap_eth_dev_bridge__ip_query_while_mac_also_changed(UNUSED void** state) {
    s_test_mac_and_ip_change_at_same_time(true);
}

void test_gmap_eth_dev_bridge__multiple_sockets_mac_change(UNUSED void** state) {
    gmap_eth_dev_bridge_collection_t* bridges = NULL;
    gmap_query_t query_macip = {0};
    mock_discoping_t mock_ext_discoping1 = { .keep_log = true };
    mock_discoping_t mock_ext_discoping2 = { .keep_log = true };
    mock_discoping_t mock_ext_discoping3 = { .keep_log = true };
    mock_discoping_t mock_ext_discoping4 = { .keep_log = true };
    amxc_var_t* gmap_bridge = NULL;

    // GIVEN an discoping and a gmap bridge with two IPs and a mac
    s_create_bridge_and_discoping_ext(&mock_ext_discoping1, &bridges, &query_macip, false);
    gmap_bridge = s_retval_of_gmap_get("bridge1", "bridge_netdevname", 1, NULL);
    test_util_parse_odl("gmap-mockdata-bridge-ip.odl");
    test_util_parse_odl("gmap-mockdata-bridge-ipv6.odl");
    test_util_handle_events();
    will_return(__wrap_discoping_new, &mock_ext_discoping1);
    s_expect_bridge_modules(AF_INET);
    will_return(__wrap_discoping_new, &mock_ext_discoping2);
    s_expect_bridge_modules(AF_INET6);
    s_bridge_receives_first_new_mac(&query_macip, gmap_bridge);

    // WHEN the bridge receives a new MAC
    will_return(__wrap_discoping_new, &mock_ext_discoping3);
    s_expect_bridge_modules(AF_INET);
    will_return(__wrap_discoping_new, &mock_ext_discoping4);
    s_expect_bridge_modules(AF_INET6);
    s_bridge_changes_mac(&query_macip, gmap_bridge, "gmap-mockdata-bridge-mac2.odl", 1, "0b:07:01:0d:09:02");
    mock_discoping_assert_log(&mock_ext_discoping1, 0, "open", "0b:07:01:0d:09:03", "192.168.1.1", "bridge_netdevname");
    mock_discoping_assert_log(&mock_ext_discoping2, 0, "open", "0b:07:01:0d:09:03", "2a02:1802:94:43c0::1", "bridge_netdevname");

    // THEN it closed the old two socket on which it must not send anymore.
    mock_discoping_assert_log(&mock_ext_discoping1, 1, "close", "0b:07:01:0d:09:03", "192.168.1.1", "bridge_netdevname");
    mock_discoping_assert_log(&mock_ext_discoping2, 1, "close", "0b:07:01:0d:09:03", "2a02:1802:94:43c0::1", "bridge_netdevname");

    // AND THEN it opened the two new sockets
    mock_discoping_assert_log(&mock_ext_discoping3, 0, "open", "0b:07:01:0d:09:02", "192.168.1.1", "bridge_netdevname");
    mock_discoping_assert_log(&mock_ext_discoping4, 0, "open", "0b:07:01:0d:09:02", "2a02:1802:94:43c0::1", "bridge_netdevname");

    // AND THEN it did nothing else
    assert_int_equal(2, amxc_llist_size(amxc_var_constcast(amxc_llist_t, &mock_ext_discoping1.log)));
    assert_int_equal(2, amxc_llist_size(amxc_var_constcast(amxc_llist_t, &mock_ext_discoping2.log)));
    assert_int_equal(1, amxc_llist_size(amxc_var_constcast(amxc_llist_t, &mock_ext_discoping3.log)));
    assert_int_equal(1, amxc_llist_size(amxc_var_constcast(amxc_llist_t, &mock_ext_discoping4.log)));

    expect_value(__wrap_gmap_query_close, query, &query_macip);

    gmap_eth_dev_bridge_collection_delete(&bridges);
    amxc_var_delete(&gmap_bridge);
    amxc_var_clean(&mock_ext_discoping1.log);
    amxc_var_clean(&mock_ext_discoping2.log);
    amxc_var_clean(&mock_ext_discoping3.log);
    amxc_var_clean(&mock_ext_discoping4.log);
}
