/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__GMAP_ETH_DEV_MOCK_EXT_ARPING_H__)
#define __GMAP_ETH_DEV_MOCK_EXT_ARPING_H__

#include <discoping/discoping.h>

typedef struct {
    /** currently open one, otherwise NULL */
    char* netdevname;
    /** currently open one, otherwise NULL */
    char* mac;
    /** currently open one, otherwise NULL */
    char* ip;
    int32_t open_count;
    /** only actual closes (does not count when closing while not opened) */
    int close_count;
    discoping_up_cb_t up_cb;
    discoping_up_cb_t down_cb;
    discoping_collision_cb_t collision_cb;
    void* userdata;
    void* leak_canary;
    /**
     * Of type @ref AMXC_VAR_ID_LIST with elements of type @ref AMXC_VAR_ID_HTABLE
     * with keys "mac", "ip", "netdev_name", "type". "type" has possible values "open" and "close".
     */
    amxc_var_t log;
    /** If set to true, don't destroy the log when the discoping instance is deleted. */
    bool keep_log;
} mock_discoping_t;

typedef struct gmap_eth_dev_discoping gmap_eth_dev_discoping_t;

bool __wrap_discoping_new(discoping_t** discoping, amxo_parser_t* parser);
void __wrap_discoping_delete(discoping_t** discoping);

bool __wrap_discoping_open(discoping_t* discoping, const char* netdev_name, const char* mac, const char* ip);
bool __wrap_discoping_close(discoping_t* discoping, const char* netdev_name, const char* mac, const char* ip);

void __wrap_discoping_set_on_up_cb(discoping_t* discoping, discoping_up_cb_t up_cb);
void __wrap_discoping_set_on_down_cb(discoping_t* discoping, discoping_down_cb_t down_cb);
void __wrap_discoping_set_on_collision_cb(discoping_t* discoping, discoping_collision_cb_t collision_cb);

void __wrap_discoping_set_userdata(discoping_t* discoping, void* userdata);

void mock_discoping_assert(mock_discoping_t* mock, int expected_opencount, int expected_closecount, const char* expected_devname, const char* expected_mac, const char* expected_ip);

void __wrap_discoping_verify(discoping_t* discoping, const char* ip, const char* mac);
void mock_discoping_expect_verify(const char* ip, const char* mac);

void __wrap_discoping_verify_once(discoping_t* discoping, const char* ip, const char* mac);
void mock_discoping_expect_verify_once(const char* ip, const char* mac);

void __wrap_discoping_mark_seen_network(discoping_t* discoping, const char* ip, const char* mac);
void mock_discoping_expect_mark_seen_network(const char* ip, const char* mac);

void __wrap_discoping_set_periodic_reverify_enabled(discoping_t* discoping, const char* ip, bool enabled);
void mock_discoping_expect_set_periodic_reverify_enabled(const char* ip, bool enabled);

amxc_var_t* __wrap_discoping_get_open_sockets(discoping_t* discoping);
void mock_discoping_expect_get_open_sockets_none(void);
void mock_discoping_expect_get_open_sockets_one(const char* netdev_name, const char* ip, const char* mac);

void mock_discoping_assert_log(mock_discoping_t* mock_ext_discoping,
                               int64_t index,
                               const char* expected_type,
                               const char* expected_mac,
                               const char* expected_ip,
                               const char* expected_netdev_name);

gmap_eth_dev_discoping_t* mock_discoping_new(mock_discoping_t* discoping);
void mock_discoping_delete(gmap_eth_dev_discoping_t** discoping_gmap,
                           mock_discoping_t* discoping);

#endif