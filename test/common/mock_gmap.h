/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__GMAP_ETH_DEV_MOCK_GMAP_H__)
#define __GMAP_ETH_DEV_MOCK_GMAP_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxb/amxb_types.h>
#include <amxb/amxb_be.h>
#include <amxo/amxo.h>

#include <gmap/gmap.h>
#include <stdint.h>
#include <unistd.h>

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

/**
 * Transparent type.
 */
typedef struct {
    const char* will_return_key;
    bool will_return_already_exists;
    /** returnvalue of @ref gmap_devices_createDeviceOrGetKey() */
    bool will_return_success;
    const char* expected_key1;
    const char* expected_value_string1;
    const char* expected_tags;
    const char* expected_discovery_source;
} mock_gmap_devices_create_mockdata_t;

/**
 *
 * `mock()` value is copied and must be of type @ref amxc_var_t of type @ref AMXC_VAR_ID_HTABLE.
 * Actual return value of @ref gmap_device_get will be of type @ref AMXC_VAR_ID_LIST.
 */
amxc_var_t* __wrap_gmap_device_get(const char* key, uint32_t flags);

bool __wrap_gmap_device_set(const char* key,
                            amxc_var_t* values);

bool __wrap_gmap_devices_createDevice(const char* key,
                                      const char* discovery_source,
                                      const char* tags, bool persistent,
                                      const char* default_name);
bool __wrap_gmap_devices_createDevice_ext(const char* key,
                                          const char* discovery_source,
                                          const char* tags, bool persistent,
                                          const char* default_name, amxc_var_t* values);

/**
 *
 * `mock()` value is of type @ref mock_gmap_devices_create_mockdata_t and freed by callee.
 */
bool __wrap_gmap_devices_createDeviceOrGetKey(char** target_key,
                                              bool* target_exists,
                                              const char* mac,
                                              const char* discovery_source,
                                              const char* tags, bool persistent,
                                              const char* default_name, amxc_var_t* values);

void mock_gmap_expect_createDeviceOrGetKey(const char* mac, const mock_gmap_devices_create_mockdata_t* mockdata);

bool __wrap_gmap_devices_createIfActive(
    char** tgt_dev_key,
    bool* target_exists,
    const char* mac,
    const char* discovery_source,
    const char* tags, bool persistent,
    const char* default_name, amxc_var_t* values, bool active);

void mock_gmap_expect_createIfActive(const char* mac, const mock_gmap_devices_create_mockdata_t* mockdata, bool active);

/**
 *
 * `mock()` value is freed and must be of type @ref amxc_var_t of type @ref AMXC_VAR_ID_LIST with
 *   items of type @ref AMXC_VAR_ID_CSTRING.
 *
 * Contrary to the above but consistent with actual implementation,
 * value put in `ret` will be of type @ref AMXC_VAR_ID_LIST with items of type @ref AMXC_VAR_ID_LIST
 *   (list in list) with items of type @ref AMXC_VAR_ID_CSTRING.
 */
bool __wrap_gmap_devices_find(const char* expression, uint32_t flags, amxc_var_t* ret);

char* __wrap_gmap_devices_findByMac(const char* mac);

void mock_gmap_expect_findByMac(const char* mac, const char* expected_key);

char* __wrap_gmap_devices_findByIp(const char* mac);

bool __wrap_gmap_device_setTag(const char* key, const char* tags, const char* expression, gmap_traverse_mode_t mode);
void mock_gmap_expect_setTag(const char* key, const char* tags, const char* expression, gmap_traverse_mode_t mode);


bool __wrap_gmap_devices_linkAdd(const char* upper,
                                 const char* lower,
                                 const char* type,
                                 const char* datasource,
                                 uint32_t priority);

void mock_gmap_expect_devices_linkAdd(const char* upper, const char* lower, const char* type, const char* datasource);


bool __wrap_gmap_devices_linkReplace(const char* upper,
                                     const char* lower,
                                     const char* type,
                                     const char* datasource,
                                     uint32_t priority);

void mock_gmap_expect_devices_linkReplace(const char* upper, const char* lower, const char* type, const char* datasource);

bool __wrap_gmap_devices_linkRemove(const char* upper,
                                    const char* lower,
                                    const char* datasource);

gmap_query_t* __wrap_gmap_query_open_ext(const char* expression, const char* name,
                                         gmap_query_cb_t fn, void* user_data);
bool __wrap_gmap_query_open_ext_2(gmap_query_t** query,
                                  const char* expression,
                                  const char* name,
                                  gmap_query_flags_t flags,
                                  gmap_query_cb_t fn,
                                  void* user_data);

void __wrap_gmap_query_close(gmap_query_t* query);

amxd_status_t __wrap_gmap_ip_device_delete_address(const char* key, uint32_t family, const char* address);

void mock_gmap_expect_ip_device_delete_address(const char* key, uint32_t family, const char* address, amxd_status_t will_return_status);

bool __wrap_gmap_device_setActive(const char* key, bool value, const char* expect_source, uint32_t priority);

void mock_gmap_expect_setActive(const char* expect_key, bool expect_active, bool return_value);

bool __wrap_gmap_device_clearTag(const char* key, const char* tags, const char* expression, gmap_traverse_mode_t mode);

bool __wrap_gmap_device_setName(const char* key,
                                const char* name,
                                const char* source);

amxd_status_t __wrap_gmap_ip_device_add_address(const char* key, uint32_t family, const char* address,
                                                const char* scope, const char* status_value, const char* address_source, bool reserved);

void mock_gmap_expect_ip_device_add_address(const char* key, uint32_t family, const char* address,
                                            const char* scope, const char* status_value, const char* address_source, bool reserved, amxd_status_t return_value);

amxd_status_t __wrap_gmap_ip_device_set_address(const char* key, uint32_t family,
                                                const char* address, const char* scope, const char* status_value, const char* address_source,
                                                bool reserved);

void mock_gmap_expect_ip_device_set_address(const char* key, uint32_t family, const char* address,
                                            const char* scope, const char* status_value, const char* address_source, bool reserved,
                                            amxd_status_t return_value);

amxd_status_t __wrap_gmap_ip_device_get_address(const char* key, uint32_t family,
                                                const char* address, amxc_var_t* const ret_object);

/**
 *
 * @param ret_object Data to be copied to `ret_object` parameter of @ref gmap_ip_device_get_address.
 *   Caller (i.e. unittest) must not free this. Will be freed by mock function.
 *
 *   The reason the caller must not free this is because test helper functions that call this
 *   function cannot free it (because it's in use after the test helper function returns),
 *   and we don't want to have the unit test do bookkeeping of what of which test helper function
 *   has to be freed.
 */
void mock_gmap_expect_ip_device_get_address(const char* key, uint32_t family,
                                            const char* address, amxc_var_t* ret_object);

amxd_status_t __wrap_gmap_ip_device_get_addresses(const char* key, amxc_var_t* target_addresses);

/**
 *
 * @param gmap_addresses_json_file filename containing data in format `{ip1 => { key: value, ...}, ip2 => { ... }, ...}`.
 */
void mock_gmap_ip_device_get_addresses(const char* key, const char* gmap_addresses_json_file);

amxc_var_t* __wrap_gmap_config_get(const char* module, const char* option);


typedef struct gmap_eth_dev_bridgetable gmap_eth_dev_bridgetable_t;
typedef struct gmap_eth_dev_dhcp_reader gmap_eth_dev_dhcp_reader_t;
typedef struct gmap_eth_dev_neighbors gmap_eth_dev_neighbors_t;
typedef struct gmap_eth_dev_bridge gmap_eth_dev_bridge_t;

void __wrap_gmap_eth_dev_bridgetable_new(gmap_eth_dev_bridgetable_t** instance, const gmap_eth_dev_bridge_t* bridge);
void __wrap_gmap_eth_dev_dhcp_reader_new(gmap_eth_dev_dhcp_reader_t** instance, const gmap_eth_dev_bridge_t* bridge);
void __wrap_gmap_eth_dev_neighbors_new(gmap_eth_dev_neighbors_t** instance, const gmap_eth_dev_bridge_t* bridge);

#ifdef __cplusplus
}
#endif

#endif