/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "mock_netmodel.h"
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_types.h>
#include <string.h>
#include <debug/sahtrace.h>

#include <amxb/amxb_types.h>
#include <unistd.h>
#include <malloc.h>
#include "test-util.h"

struct _netmodel_query {
    amxc_htable_it_t it;
    netmodel_callback_t cb;
    void* userdata;
};

static void s_clean_netmodel_query(const char* key, amxc_htable_it_t* it) {
    netmodel_query_t* query = NULL;

    assert_non_null(key);
    assert_non_null(it);

    query = amxc_htable_it_get_data(it, netmodel_query_t, it);
    free(query);
}

static struct {
    bool initialized;
    amxc_htable_t queries;
    amxc_htable_t states;
} netmodel = { 0 };

bool __wrap_netmodel_initialize(void) {
    function_called();
    assert_false(netmodel.initialized);
    amxc_htable_init(&netmodel.queries, 0);
    amxc_htable_init(&netmodel.states, 0);
    netmodel.initialized = true;
    return true;
}

void __wrap_netmodel_cleanup(void) {
    function_called();
    if(netmodel.initialized) {
        assert_true(amxc_htable_is_empty(&netmodel.queries));

        amxc_htable_clean(&netmodel.queries, s_clean_netmodel_query);
        amxc_htable_clean(&netmodel.states, variant_htable_it_free);
        netmodel.initialized = false;
    }
}

static void s_handle_events(const char* key) {
    amxc_htable_it_t* query_it = amxc_htable_get(&netmodel.queries, key);
    netmodel_query_t* query = query_it == NULL ? NULL : amxc_htable_it_get_data(query_it, netmodel_query_t, it);
    amxc_htable_it_t* var_it = amxc_htable_get(&netmodel.states, key);
    amxc_var_t* var = var_it == NULL ? NULL : amxc_var_from_htable_it(var_it);
    when_null(query, out);

    if(var == NULL) {
        amxc_var_new(&var);
        amxc_htable_insert(&netmodel.states, key, &var->hit);
    }

    query->cb(key, var, query->userdata);

out:
    return;
}

netmodel_query_t* __wrap_netmodel_openQuery_getFirstParameter(const char* intf,
                                                              const char* subscriber,
                                                              const char* name,
                                                              const char* flag,
                                                              const char* traverse,
                                                              netmodel_callback_t handler,
                                                              void* userdata) {
    const char* key = NULL;
    netmodel_query_t* item = NULL;

    check_expected(intf);
    check_expected(subscriber);
    check_expected(name);
    check_expected(flag);
    check_expected(traverse);
    assert_non_null(handler);
    assert_true(netmodel.initialized);


    key = (const char* ) mock();
    item = calloc(1, sizeof(netmodel_query_t));
    item->cb = handler;
    item->userdata = userdata;
    assert_int_equal(0, amxc_htable_insert(&netmodel.queries, key, &(item->it)));

    s_handle_events(key);

    return item;
}

void __wrap_netmodel_closeQuery(netmodel_query_t* query) {
    assert_non_null(query);
    assert_true(netmodel.initialized);

    amxc_htable_for_each(it, &netmodel.queries) {
        if(it == &query->it) {
            goto found;
        }
    }
    fail_msg("Double free on netmodel query");

    /* Unreachable */
    return;
found:
    amxc_htable_it_clean(&query->it, s_clean_netmodel_query);
}

void mock_netmodel_set_data(const char* key, const amxc_var_t* const var) {
    assert_non_null(key);
    assert_non_null(var);
    assert_true(netmodel.initialized);

    amxc_htable_it_t* old_entry = amxc_htable_get(&netmodel.states, key);
    amxc_var_t* old_var = old_entry == NULL ? NULL : amxc_var_from_htable_it(old_entry);
    if(old_var == NULL) {
        amxc_var_new(&old_var);
        amxc_htable_insert(&netmodel.states, key, &old_var->hit);
    }
    /* Always store copies, so that we don't accidentally store dangling pointers
     * to some local test variable. */
    amxc_var_copy(old_var, var);

    s_handle_events(key);
}
