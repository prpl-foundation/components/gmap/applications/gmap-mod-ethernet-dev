/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_dhcp_clientpath.h"
#include "test_dhcp_context.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include "gmap_eth_dev_dhcp_clientpath.h"
#include "gmap_eth_dev_dhcp.h"
#include "gmap_eth_dev.h"
#include <string.h>
#include <sys/socket.h>
#include <stdio.h>
#include <amxc/amxc.h> // needed for <amxp/amxp.h>
#include <amxp/amxp.h> // needed for <amxd/amxd_object.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <malloc.h>

#include "../common/test-setup.h"
#include "../common/test-util.h"
#include "../common/mock_gmap.h"
#include "../common/mock_discoping.h"

void test_gmap_eth_dev_dhcp_clientpath_set__device_existed(UNUSED void** state) {
    // GIVEN nothing

    // EXPECT device will be created and already exists
    mock_gmap_expect_createDeviceOrGetKey("11:22:33:44:55:66", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "my_id",
        .will_return_already_exists = true,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));
    mock_gmap_expect_setTag("my_id", "ipv4 dhcp", NULL, gmap_traverse_this);

    // EXPECT dhcp client path will be set
    expect_string(__wrap_gmap_device_set, key, "my_id");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.123.Client.456.");

    // WHEN setting dhcp client path
    gmap_eth_dev_dhcp_clientpath_set("11:22:33:44:55:66", GMAP_DHCPV4SERVER_PATH "Pool.123.Client.456.");
}

void test_gmap_eth_dev_dhcp_clientpath_set__device_did_not_exist(UNUSED void** state) {
    // GIVEN nothing

    // EXPECT device will be created and did not exist yet
    mock_gmap_expect_createDeviceOrGetKey("11:22:33:44:55:66", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "my_id",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));

    // EXPECT dhcp client path will be set
    expect_string(__wrap_gmap_device_set, key, "my_id");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.123.Client.456.");

    // WHEN setting dhcp client path
    gmap_eth_dev_dhcp_clientpath_set("11:22:33:44:55:66", GMAP_DHCPV4SERVER_PATH "Pool.123.Client.456.");
}

void test_gmap_eth_dev_dhcp_clientpath_set__device_creation_failed(UNUSED void** state) {
    // GIVEN nothing

    // EXPECT device creation will fail
    mock_gmap_expect_createDeviceOrGetKey("11:22:33:44:55:66", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "my_id",
        .will_return_already_exists = false,
        .will_return_success = false,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));

    // WHEN setting dhcp client path
    gmap_eth_dev_dhcp_clientpath_set("11:22:33:44:55:66", GMAP_DHCPV4SERVER_PATH "Pool.123.Client.456.");

    // THEN setting clientpath is aborted and nothing else happens
}

void test_gmap_eth_dev_dhcp_clientpath_set__unset_normal_via_NULL(UNUSED void** state) {
    // GIVEN nothing

    // EXPECT device lookup will succeed
    mock_gmap_expect_findByMac("11:22:33:44:55:66", "mykey");

    // EXPECT dhcp client path will be cleared
    expect_string(__wrap_gmap_device_set, key, "mykey");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "");

    // WHEN clearing dhcp client path by passing nullpointer
    gmap_eth_dev_dhcp_clientpath_set("11:22:33:44:55:66", NULL);
}

void test_gmap_eth_dev_dhcp_clientpath_set__unset_normal_via_empty(UNUSED void** state) {
    // GIVEN nothing

    // EXPECT device lookup will succeed
    mock_gmap_expect_findByMac("11:22:33:44:55:66", "mykey");

    // EXPECT dhcp client path will be cleared
    expect_string(__wrap_gmap_device_set, key, "mykey");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "");

    // WHEN clearing dhcp client path by passing empty string
    gmap_eth_dev_dhcp_clientpath_set("11:22:33:44:55:66", "");
}

void test_gmap_eth_dev_dhcp_clientpath_set__unset_with_no_device(UNUSED void** state) {
    // GIVEN nothing

    // EXPECT device lookup will fail
    mock_gmap_expect_findByMac("11:22:33:44:55:66", NULL);

    // WHEN setting dhcp client path
    gmap_eth_dev_dhcp_clientpath_set("11:22:33:44:55:66", NULL);

    // THEN nothing happens
}

void test_gmap_eth_dev_dhcp_clientpath__scenario(UNUSED void** state) {
    // GIVEN a dhcp server, two dhcp pools, and a gmap_eth_dev_dhcp_reader (by test-setup)
    test_util_parse_odl("dhcpv4-mockdata-pool-1.odl");
    test_util_handle_events();
    test_util_parse_odl("dhcpv4-mockdata-pool-1.odl"); // second pool, same bridge
    test_util_handle_events();

    // ---------------

    // EXPECT first device will be created
    mock_gmap_expect_createDeviceOrGetKey("0a:0B:0c:0D:EF:01", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "dev1",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));

    // EXPECT dhcp client path will be set
    expect_string(__wrap_gmap_device_set, key, "dev1");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.1.Client.1.");

    // WHEN new client appears
    test_util_parse_odl("dhcpv4-mockdata-client1.odl");
    test_util_handle_events();

    // ---------------

    // EXPECT second device will be created
    mock_gmap_expect_createDeviceOrGetKey("0a:0B:0c:0D:EF:02", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "dev2",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));

    // EXPECT dhcp client path will be set, of correct device, with correct path
    expect_string(__wrap_gmap_device_set, key, "dev2");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.1.Client.2.");

    // WHEN a second client appears
    test_util_parse_odl("dhcpv4-mockdata-client2.odl");
    test_util_handle_events();

    // ---------------

    // EXPECT third device will be created
    mock_gmap_expect_createDeviceOrGetKey("0a:0B:0c:0D:EF:03", (&(mock_gmap_devices_create_mockdata_t) {
        .will_return_key = "dev3",
        .will_return_already_exists = false,
        .will_return_success = true,
        .expected_tags = "lan edev mac physical ipv4 dhcp",
    }));

    // EXPECT dhcp client path will be set, of correct device, with path in second pool
    expect_string(__wrap_gmap_device_set, key, "dev3");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "Device.DHCPv4.Server.Pool.2.Client.1.");

    // WHEN a client appears in the second dhcp pool
    test_util_parse_odl("dhcpv4-mockdata-client-in-pool2.odl");
    test_util_handle_events();


    // ---------------

    // EXPECT device will be looked up
    mock_gmap_expect_findByMac("0a:0B:0c:0D:EF:02", "dev2");

    // EXPECT dhcp client path will be cleared
    expect_string(__wrap_gmap_device_set, key, "dev2");
    expect_check(__wrap_gmap_device_set, values, test_util_has_expected_dhcpv4client, "");

    // WHEN a client disappears
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, GMAP_DHCPV4SERVER_PATH "Pool.1.Client.");
    amxd_trans_del_inst(&trans, 2, NULL);
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, get_dummy_dm()));
    amxd_trans_clean(&trans);
    test_util_handle_events();
}
