/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_dhcp_context.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include "gmap_eth_dev_bridge.h"
#include "gmap_eth_dev_dhcp_clientpath.h"
#include "gmap_eth_dev_dhcp.h"
#include "gmap_eth_dev_discoping.h"
#include "gmap_eth_dev.h"
#include <string.h>
#include <sys/socket.h>
#include <stdio.h>
#include <amxc/amxc.h> // needed for <amxp/amxp.h>
#include <amxp/amxp.h> // needed for <amxd/amxd_object.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <malloc.h>

#include "../common/test-setup.h"
#include "../common/test-util.h"
#include "../common/mock_gmap.h"
#include "../common/mock_discoping.h"
#include "../common/mock_netmodel.h"

#define NETMODEL_DATA_KEY "netmodel.query.key.0"
static mock_discoping_t mock_ext_discoping = {0};
static gmap_eth_dev_bridge_collection_t bridges = { 0 };
static gmap_eth_dev_bridge_t bridge = {
    .netdev = NULL,
    .netdev_index = 5,
    .proto_family = AF_INET,
    .mac = (char*) "60:50:40:30:20:10",
};
static gmap_eth_dev_discoping_t* discoping = NULL;
static gmap_eth_dev_dhcp_reader_t* test_dhcp_reader = NULL;

void test_setup_dhcp_reader(const char* bridge_key, const char* bridge_devname, const char* bridge_pool) {
    amxc_var_t netmodel_iface_var;

    assert_non_null(bridge_devname);
    assert_string_not_equal(bridge_devname, "");
    assert_null(discoping);
    assert_null(bridge.collection);
    assert_null(bridge.netdev);
    assert_null(bridge.dhcp);
    assert_non_null(gmap_eth_dev_get_ctx_dhcp()); /* gmap_eth_dev must have been initialized already */

    amxc_var_init(&netmodel_iface_var);
    amxc_var_set(cstring_t, &netmodel_iface_var, bridge_pool);
    mock_netmodel_set_data(NETMODEL_DATA_KEY, &netmodel_iface_var);

    test_util_parse_odl("device-ip-dm.odl");
    test_util_parse_odl("device-ip-mockdata.odl");

    discoping = test_util_new_discoping_ext(&mock_ext_discoping, true);
    amxc_htable_init(&bridges.bridges, 1);
    bridge.discoping = discoping;
    bridge.collection = &bridges;
    bridge.netdev = strdup(bridge_devname);
    amxc_htable_insert(&bridges.bridges, bridge_key, &bridge.it);

    expect_string(__wrap_netmodel_openQuery_getFirstParameter, intf, BRIDGE_DEFAULT_KEY);
    expect_string(__wrap_netmodel_openQuery_getFirstParameter, subscriber, "gmap-mod-eth-dev_bridge-mybridge_2");
    expect_string(__wrap_netmodel_openQuery_getFirstParameter, name, "InterfacePath");
    expect_string(__wrap_netmodel_openQuery_getFirstParameter, flag, "ip");
    expect_string(__wrap_netmodel_openQuery_getFirstParameter, traverse, "up");
    will_return(__wrap_netmodel_openQuery_getFirstParameter, NETMODEL_DATA_KEY);

    gmap_eth_dev_dhcp_reader_new(&test_dhcp_reader, &bridge);
    assert_non_null(test_dhcp_reader);
    test_util_handle_events();

    amxc_var_clean(&netmodel_iface_var);
}

void test_setup_dhcp_reader_default(void) {
    test_setup_dhcp_reader(BRIDGE_DEFAULT_KEY, BRIDGE_DEFAULT_DEVNAME, BRIDGE_DEFAULT_POOL);
}

void test_setup_teardown_dhcp_reader(void) {
    gmap_eth_dev_dhcp_reader_delete(&test_dhcp_reader);
    gmap_eth_dev_discoping_delete(&discoping);
    free(bridge.netdev);
    amxc_htable_clean(&bridges.bridges, NULL);

    bridge.netdev = NULL;
    bridge.collection = NULL;
    bridge.discoping = NULL;
    discoping = NULL;
    test_dhcp_reader = NULL;
    mock_ext_discoping = (mock_discoping_t) {};
}

int test_setup_full(void** state) {
    test_setup(state);
    amxp_sigmngr_trigger_signal(NULL, "wait:done", NULL);
    test_util_parse_odl("dhcpv4-dm.odl");
    test_setup_dhcp_reader_default();
    return 0;
}

int test_teardown_full(void** state) {
    test_setup_teardown_dhcp_reader();
    test_teardown_query(state);
    return 0;
}
