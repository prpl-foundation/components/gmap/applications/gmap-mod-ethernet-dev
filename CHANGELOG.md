# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.20.7 - 2024-11-13(18:55:08 +0000)

### Other

- Let initial fetch of netdev neigh not ignore stale IPs

## Release v1.20.6 - 2024-11-08(12:45:16 +0000)

### Other

- Optimize gmap application query events

## Release v1.20.5 - 2024-10-24(10:22:01 +0000)

### Other

- Reduce neighbour table chatter between gmap-mod-ethernet-dev and netdev

## Release v1.20.4 - 2024-10-22(08:24:23 +0000)

### Other

- Do not create device on inactive device detection to avoid creating device after manual removal

## Release v1.20.3 - 2024-10-21(14:52:06 +0000)

### Other

- gmap-mod-ethernet-dev can't find DHCPv4Server.

## Release v1.20.2 - 2024-09-17(08:48:55 +0000)

### Other

- Extend documentation

## Release v1.20.1 - 2024-09-16(10:28:33 +0000)

### Other

- Make DHCPv4Server an optional dependency

## Release v1.20.0 - 2024-07-21(07:06:05 +0000)

### Other

- Keep "manageable" tag

## Release v1.19.2 - 2024-07-08(14:39:36 +0000)

### Fixes

- Only verify IP reachability on correct libdiscoping instance

## Release v1.19.1 - 2024-05-15(07:26:52 +0000)

### Other

- [amx][gmap][mod-eth] Remove deprecated configuration file

## Release v1.19.0 - 2024-05-14(12:36:26 +0000)

### Other

- Adapt to new link API

## Release v1.18.7 - 2024-05-14(07:57:35 +0000)

### Other

- [amx][gmap][mod-eth] Do discoping+init per bridge per ip family

## Release v1.18.6 - 2024-04-10(10:07:59 +0000)

### Changes

- Make amxb timeouts configurable

## Release v1.18.5 - 2024-03-16(08:45:03 +0000)

### Other

- workaround amxb_be_who_has("DHCPv4Server.") timeout by retrying 10x

## Release v1.18.4 - 2024-03-11(10:51:35 +0000)

### Other

- [gmap][wifi] Wifi devices in gmap have the 'eth' tag instead of the 'wifi' tag

## Release v1.18.3 - 2024-02-21(08:44:49 +0000)

### Other

- Do not load gmap-server config since gmap-server does that by itself now

## Release v1.18.2 - 2024-02-20(11:44:50 +0000)

### Other

- Install config defaults in /etc/amx/ instead of in /etc/config/

## Release v1.18.1 - 2024-02-06(14:00:51 +0000)

### Other

- do discoping on bridgetable entry reappearance

## Release v1.18.0 - 2024-01-11(09:49:22 +0000)

### Other

- Sync Layer1Interface from parent interface to edev device

## Release v1.17.1 - 2024-01-08(15:35:41 +0000)

### Fixes

- Fix ipv4 discoping not robust when low gmap/system performance

## Release v1.17.0 - 2024-01-08(07:53:49 +0000)

### Other

- One query instead of two overlapping ones

## Release v1.16.1 - 2023-11-15(16:56:13 +0000)

### Other

- Support wifi devices + also update Active on link error

## Release v1.16.0 - 2023-10-30(09:10:09 +0000)

### Other

- [amx][gmap] move non-generic functions out of libgmap-client add libgmap-ext dep

## Release v1.15.1 - 2023-10-19(14:36:16 +0000)

### Other

- Change bridgetable Active priority to 50

## Release v1.15.0 - 2023-09-19(13:05:25 +0000)

### Other

- Keep link to port on disconnect

## Release v1.14.1 - 2023-09-19(12:57:54 +0000)

### Other

- Use link() instead of setLink()

## Release v1.14.0 - 2023-09-14(08:10:31 +0000)

### Other

- Support changing MAC while gmap is running

## Release v1.13.0 - 2023-08-31(09:02:20 +0000)

### Other

- adapt to discoping moved to libdiscoping

## Release v1.12.0 - 2023-06-15(09:59:26 +0000)

### Other

- update documentation

## Release v1.11.0 - 2023-06-15(08:17:55 +0000)

### Other

- ipv6 for discoping and neighbors

## Release v1.10.3 - 2023-05-30(09:47:31 +0000)

### Other

- Fix collision detection late when IP from other source

## Release v1.10.2 - 2023-04-17(09:09:54 +0000)

### Other

- Removed deprecated odl keywords in tests

## Release v1.10.1 - 2023-04-05(08:06:38 +0000)

### Other

- Renames for discoping

## Release v1.10.0 - 2023-03-21(14:23:55 +0000)

### Other

- Listen to NetDev.Link.<port>.Neigh

## Release v1.9.1 - 2023-03-20(11:27:51 +0000)

### Other

- Rename gmap_ethernet_dev_netdev->gmap_ethernet_dev_bridgetable

## Release v1.9.0 - 2023-03-15(15:26:49 +0000)

### Other

- Adapt to DHCPv4Server path change

## Release v1.8.0 - 2023-02-24(14:25:28 +0000)

### Other

- Provide a parameter reference to the DHCP Client Lease

## Release v1.7.3 - 2023-02-23(10:57:18 +0000)

### Other

- Adapt to new setActive API with source and priority

## Release v1.7.2 - 2023-02-15(08:35:12 +0000)

### Other

- Adapt to 'nemo' tag not used anymore

## Release v1.7.1 - 2023-02-07(08:59:40 +0000)

### Other

- Mark colliding IPs with Status=error

## Release v1.7.0 - 2023-01-23(08:38:22 +0000)

### Other

- Support ARP IP down + start arping

## Release v1.6.0 - 2023-01-05(11:52:17 +0000)

### New

- [import-dbg] Disable import-dbg by default for all amxrt plugin

## Release v1.5.0 - 2023-01-03(09:21:24 +0000)

### Other

- Create IP in gmap device on gratuitous ARP

## Release v1.4.0 - 2022-11-29(14:13:45 +0000)

### Other

- Rename name source 'HostName'->'dhcp'

## Release v1.3.1 - 2022-11-23(13:42:10 +0000)

### Other

- Add DHCP option 12 HostName

## Release v1.3.0 - 2022-11-07(15:26:17 +0000)

### Other

- Talk to standard TR-181 DHCPv4 datamodel instead of custom notifications

## Release v1.2.2 - 2022-09-27(11:30:43 +0000)

### Other

- Fix fail to subscribe to DHCPv4 at boot

## Release v1.2.1 - 2022-09-26(12:43:24 +0000)

### Other

- fix dhcp: no IP addresses created on new lease

## Release v1.2.0 - 2022-09-23(07:11:36 +0000)

### Other

- Various cleanup

## Release v1.1.0 - 2022-09-15(07:59:34 +0000)

### Other

- replace using netlink with using NetDev + test

## Release v1.0.3 - 2022-08-30(10:15:39 +0000)

### Other

- Add missing requires

## Release v1.0.2 - 2022-08-09(08:17:36 +0000)

### Other

- Link dev<->port dynamically when all info is available

## Release v1.0.1 - 2022-08-04(07:16:34 +0000)

### Other

- LAN Device must have a uuid as unique (instance)key

## Release v1.0.0 - 2022-07-26(08:39:35 +0000)

### Other

- [prpl][gMap] gMap needs to be split in a gMap-core and gMap-client process

## Release v0.1.4 - 2022-07-15(06:03:54 +0000)

### Other

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.1.3 - 2022-07-14(09:07:53 +0000)

### Other

- Revert "common: add workaround method for retrieving intf from ifindex"

## Release v0.1.2 - 2022-05-30(06:29:24 +0000)

### Other

- Fix DHCP option 60 (vendor class) not coming through

## Release v0.1.1 - 2021-11-02(12:16:49 +0000)

### Changes

- remove symlink to init script

## Release v0.1.0 - 2021-09-01(11:58:34 +0000)

### New

- make use of libdhcpoption

## Release v0.0.7 - 2021-08-09(07:54:45 +0000)

### New

- GMAP-MOD-ETHERNET-DEV add dhcp information to device

## Release v0.0.6 - 2021-07-20(09:59:55 +0000)

### Fixes

- relative path in odl to .so file fails

## Release v0.0.5 - 2021-07-20(09:05:23 +0000)

### Changes

- change rights on odl

### Fix

- relative path in odl to .so file fails 

## Release v0.0.4 - 2021-04-21(09:53:02 +0000)

### Fixes

- Update gitlab-ci test deps

### Changes

- Update changelog
- Update gitlab-ci file

### Other

- [GMAP] add unit tests to gmap-mod-ethernet-device
- [GMAP] prepare gmap-mod-ethernet-dev for opensourcing

## Release 0.0.3 - 2021-04-13(11:35:22 +0200)

### Changes

- Modules and plugins should not have a version extension in their names

## Release 0.0.2 - 2021-04-12(09:27:22 +0200)

### Fixes

- Added a 15 second delay to fix the race condition with gmap-mod-self startup.
- Switch to -std=gnu99 fixes uclibc build on CI

## Release 0.0.1 - 2021-04-08(11:06:48 +0200)

### New

- Initial release
