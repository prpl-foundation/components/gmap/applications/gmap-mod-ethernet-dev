/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_eth_dev_dhcp_clientpath.h"
#include "gmap_eth_dev_dhcp.h"
#include "gmap_eth_dev.h"
#include "gmap_eth_dev_includes.h"
#include "gmap_eth_dev_util.h"
#include <amxb/amxb_subscribe.h>
#include <amxd/amxd_object_event.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxb/amxb.h>
#include <amxd/amxd_path.h>
#include <gmap/gmap_devices.h>
#include <gmap/extensions/ip/gmap_ext_ipaddress.h>
#include <string.h>
#include <stdlib.h>

/**
 * Translates DHCPv4 server's client path to TR181 path
 *
 * Translate e.g. `DHCPv4Server.Pool.123.Client.456.`
 * to `Device.DHCPv4.Server.Pool.123.Client.456.`.
 */
static void s_translate_path(amxc_string_t* target_tr181device_path, const char* source_dhcp_client_path) {
    if((source_dhcp_client_path != NULL) && (source_dhcp_client_path[0] != '\0')) {
        const char* path_without_any_prefix = NULL;
        when_false_trace(strlen(source_dhcp_client_path) > strlen(GMAP_DHCPV4SERVER_PATH),
                         exit, ERROR, "Expected DHCP client path '%s' to start with '%s'",
                         source_dhcp_client_path, GMAP_DHCPV4SERVER_PATH);
        path_without_any_prefix = source_dhcp_client_path + strlen(GMAP_DHCPV4SERVER_PATH);
        amxc_string_setf(target_tr181device_path, "Device.DHCPv4.Server.%s", path_without_any_prefix);
    } else {
        amxc_string_clean(target_tr181device_path);
    }

exit:
    return;
}

void gmap_eth_dev_dhcp_clientpath_set(const char* mac, const char* dhcp_client_path) {
    amxc_string_t tr181_path;
    amxc_var_t set_parameters;
    bool ok = false;
    char* dev_key = NULL;
    bool dev_already_existed = false;
    amxc_string_init(&tr181_path, 64);
    amxc_var_init(&set_parameters);
    when_str_empty_trace(mac, exit, ERROR, "EMPTY mac");

    if((dhcp_client_path != NULL) && (dhcp_client_path[0] != '\0')) {
        ok = gmap_devices_createDeviceOrGetKey(&dev_key, &dev_already_existed, mac,
                                               GMAP_DISCOVERY_SOURCE_DEVICE_DHCP,
                                               "lan edev mac physical ipv4 dhcp", true, NULL, NULL);
        when_false_trace(ok, exit, ERROR, "%s Error creating device or getting key", mac);
        if(dev_already_existed) {
            ok = gmap_device_setTag(dev_key, "ipv4 dhcp", NULL, gmap_traverse_this);
            if(!ok) {
                SAH_TRACEZ_ERROR(ME, "%s %s Error setting tag!", dev_key, mac);
            }
        }
    } else {
        // In case of DHCP client remove, we could be close to max number of devices in gmap,
        // and then it would be undesired to create a new device. So do not create a new one
        // and only do something if it already existed.
        dev_key = gmap_devices_findByMac(mac);
        when_null(dev_key, exit);
    }

    amxc_var_set_type(&set_parameters, AMXC_VAR_ID_HTABLE);
    s_translate_path(&tr181_path, dhcp_client_path);
    amxc_var_add_key(cstring_t, &set_parameters, "DHCPv4Client", amxc_string_get(&tr181_path, 0));

    ok = gmap_device_set(dev_key, &set_parameters);
    when_false_trace(ok, exit, ERROR, "%s Error setting DHCPv4Client='%s'",
                     dev_key, amxc_string_get(&tr181_path, 0));

exit:
    amxc_string_clean(&tr181_path);
    amxc_var_clean(&set_parameters);
    free(dev_key);
}