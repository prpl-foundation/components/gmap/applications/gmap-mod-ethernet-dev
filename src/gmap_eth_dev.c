/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_eth_dev.h"
#include "gmap_eth_dev_bridge.h"
#include "gmap_eth_dev_dhcp_reader.h"
#include "gmap_eth_dev_dhcp.h"
#include "gmap_eth_dev_discoping.h"
#include "gmap_eth_dev_includes.h"
#include "gmap_eth_dev_util.h"

#include <amxb/amxb_be_mngr.h>
#include <amxb/amxb_connect.h>
#include <amxb/amxb_types.h>
#include <amxb/amxb_wait_for.h>
#include <amxc/amxc_htable.h>
#include <amxc/amxc_llist.h>
#include <amxc/amxc_variant.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_path.h>
#include <amxo/amxo.h>
#include <amxo/amxo_types.h>
#include <amxp/amxp_slot.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <gmap/gmap.h>
#include <gmap/gmap_devices.h>
#include <gmap/gmap_devices_flags.h>
#include <netmodel/client.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

typedef struct {
    amxd_dm_t* gmap_eth_dev_dm;     /** the gmap_eth_dev data model object */
    amxo_parser_t* parser;          /** the odl parser */
    amxb_bus_ctx_t* ctx_dhcp;
    amxb_bus_ctx_t* ctx_netdev;
    gmap_eth_dev_bridge_collection_t* bridges;
} gmap_eth_dev_t;

static gmap_eth_dev_t gmap_eth_dev = { 0 };

static void gmap_eth_dev_wait_done(UNUSED const char* const s,
                                   UNUSED const amxc_var_t* const d,
                                   UNUSED void* const p) {
    when_null(gmap_eth_dev.gmap_eth_dev_dm, exit);
    when_not_null(gmap_eth_dev.ctx_dhcp, exit);

    /* HACK / workaround
     *
     * sometimes amxb_be_who_has("DHCPv4Server.") returns NULL. At that moment, there is an error log
     *   gmap-mod-ethernet-dev[3061406976]: pcb     - [x]Timeout (socket 14 - request id 5) - no reply available - (pcb_waitForReply@core/pcb_main.c:1987)
     * This normally causes initialization of gmap-mod-ethernet-dev to fail here.
     * So to avoid that, retry a number of times first.
     * To be replaced by a simple amxb_be_who_has() when that works reliable again.
     */
    gmap_eth_dev.ctx_dhcp = gmap_eth_dev_util_who_has_with_retries(GMAP_DHCPV4SERVER_PATH, 10);

    when_null_trace(gmap_eth_dev.ctx_dhcp, exit, ERROR, "data model not found '" GMAP_DHCPV4SERVER_PATH "' "
                    "is dhcpv4-manager running?");
    SAH_TRACEZ_WARNING(ME, "Found data model '" GMAP_DHCPV4SERVER_PATH "'");

    gmap_eth_dev_bridge_collection_dhcpserver_available(gmap_eth_dev.bridges, gmap_eth_dev.ctx_dhcp);

exit:
    amxp_slot_disconnect(NULL, "wait:done", gmap_eth_dev_wait_done);
    return;
}

static void gmap_eth_dev_deferred_wait(UNUSED const amxc_var_t* const data,
                                       UNUSED void* const priv) {
    int retval;
    /* "wait:done" signal is already created by libamxrt */
    retval = amxp_slot_connect(NULL, "wait:done", NULL, gmap_eth_dev_wait_done, NULL);
    when_failed_trace(retval, leave, ERROR, "subscribing wait failed for object '" GMAP_DHCPV4SERVER_PATH "': %d", retval);
    retval = amxb_wait_for_object(GMAP_DHCPV4SERVER_PATH);
    when_failed_trace(retval, leave, ERROR, "waiting failed for object '" GMAP_DHCPV4SERVER_PATH "'");

leave:
    return;
}

int gmap_eth_dev_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    int retval = -1;
    amxb_bus_ctx_t* ctx_gmap = NULL;

    SAH_TRACEZ_INFO(ME, "Starting gmap-mod-ethernet-dev");

    when_false_trace(gmap_eth_dev.ctx_dhcp == NULL, leave, ERROR, "Double init");

    /* We use a bus context per component (one for gmap, one for dhcp, one for netdev) to avoid
     * the following scenario during boot:
     *  1. gmap-server starts and connects to PCB bus
     *  2. gmap-server connects to ubus
     *  3. NetDev starts and connects to PCB bus
     *  4. NetDev connects to ubus
     *  5. DHCPv4 starts and connects to PCB bus
     *  6. gmap-mod-ethernet-dev sees all its dependencies are available over a bus, i.e. it
     *     stops waiting for `requires "NetModel.Intf.";` etc in its .odl file
     *  7. gmap-mod-ethernet-dev performs amxb_be_who_has("Devices.Device"); and receives
     *     the bus context (amxb_bus_ctx_t) for ubus
     *  8. gmap-mod-ethernet-dev uses this bus context to talk to DHCPv4
     *  9. failure since DHCPv4 is currently not available over ubus
     * 10. DHCPv4 connects to ubus
     */
    ctx_gmap = amxb_be_who_has("Devices.Device.");
    when_null_trace(ctx_gmap, leave, ERROR, "gMap data model not found 'Devices.Device' "
                    "is gmap-server running?");

    gmap_eth_dev.ctx_netdev = amxb_be_who_has("NetDev.Link.");
    when_null_trace(gmap_eth_dev.ctx_netdev, leave, ERROR, "data model not found 'NetDev.Link' "
                    "is netdev-plugin running?");

    gmap_eth_dev.gmap_eth_dev_dm = dm;
    gmap_eth_dev.parser = parser;

    gmap_client_init(ctx_gmap);
    if(!netmodel_initialize()) {
        /* libnetmodel is only used within DHCP-based device discovery.
         * If it fails, don't quit but still try to discover and verify devices
         * through the other means.
         */
        SAH_TRACEZ_ERROR(ME, "Failed to initialized libnetmodel client. Device discovery may be affected.");
    }
    gmap_eth_dev_bridge_collection_new(&gmap_eth_dev.bridges, gmap_eth_dev.parser);

    /* DHCPv4Server is an optional dependency, so it can't be listed among the
     * requires in the odl. This entry point is called within a callback to the
     * wait:done event for the required objects from the odl file.
     * Therefore, waiting on DHCPv4Server. should be deferred to the next cycle
     * on the event loop. This allows libamxrt to properly handle the appearance
     * of all the odl requirements without confusing libamxb by adding a signal
     * handler from within a callback for that same signal.
     */
    retval = amxp_sigmngr_deferred_call(NULL, gmap_eth_dev_deferred_wait, NULL, NULL);
    when_failed_trace(retval, leave, ERROR, "Failed to defer waiting for DHCPv4Server");

    retval = 0;

leave:
    return retval;
}

void gmap_eth_dev_cleanup(void) {
    gmap_eth_dev_bridge_collection_delete(&gmap_eth_dev.bridges);
    netmodel_cleanup();
    gmap_eth_dev.ctx_netdev = NULL;
    gmap_eth_dev.ctx_dhcp = NULL;
    gmap_eth_dev.gmap_eth_dev_dm = NULL;
    gmap_eth_dev.parser = NULL;
    amxp_slot_disconnect(NULL, "wait:done", gmap_eth_dev_wait_done);
}

amxb_bus_ctx_t* gmap_eth_dev_get_ctx_dhcp(void) {
    return gmap_eth_dev.ctx_dhcp;
}

amxb_bus_ctx_t* gmap_eth_dev_get_ctx_netdev(void) {
    return gmap_eth_dev.ctx_netdev;
}
