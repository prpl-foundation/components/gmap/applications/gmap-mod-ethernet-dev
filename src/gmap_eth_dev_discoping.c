/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_eth_dev_discoping.h"
#include "gmap_eth_dev_includes.h"
#include <gmap/extensions/ip/gmap_ext_ipaddress.h>

#include <gmap/gmap_main.h>
#include <gmap/gmap_devices_flags.h>
#include <discoping/discoping.h>
#include <gmap/gmap_query.h>
#include <gmap/gmap_devices.h>
#include <gmap/gmap_device.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <malloc.h>
#include <amxb/amxb_subscribe.h>

#include <arpa/inet.h>
#include <string.h>
#include <inttypes.h>

/**
 * Value for `Devices.Device.<key>.DiscoverySource` if device discovery through ARP.
 * Note that this is the discovery source of the device and not of the IP.
 */
#define GMAP_DISCOVERY_SOURCE_DEVICE_ARP "neigh"

#define GMAP_DISCOVERY_SOURCE_IP_STATIC "Static"
#define GMAP_DISCOVERY_SOURCE_IP_AUTO "AutoIP"


struct gmap_eth_dev_discoping {
    discoping_t* lib_discoping;
};

static bool s_is_ip_address_auto(uint32_t family, const char* address) {
    struct in_addr ipv4_addr;
    int net = 0;
    int status = 0;
    when_false(family == AF_INET, error); // auto ip only in ipv4
    when_str_empty_trace(address, error, ERROR, "NULL/Empty argument address");

    status = inet_pton(family, address, &ipv4_addr);
    when_false_trace(status == 1, error, ERROR, "Error converting addr '%s'", address);

    net = inet_netof(ipv4_addr);
    SAH_TRACEZ_INFO(ME, "IPv4 network part = %X", net);
    return (net & 0xA9FE) == 0xA9FE;

error:
    return false;
}

const char* gmap_eth_dev_discoping_ip_disco_source(uint32_t family, const char* dev_ip) {
    return s_is_ip_address_auto(family, dev_ip) ? GMAP_DISCOVERY_SOURCE_IP_AUTO
           : GMAP_DISCOVERY_SOURCE_IP_STATIC;
}

/**
 *
 * @param ip_status either @ref GMAP_IP_STATUS_REACHABLE or @ref GMAP_IP_STATUS_ERROR (but not unreachable)
 */
static void s_add_or_update_reachable_or_colliding_ip(const char* dev_ip, uint32_t family, const char* dev_mac, const char* ip_status) {
    char* dev_key = NULL;
    bool already_exists = false;
    bool ok = false;
    gmap_ip_address_scope_t scope = scope_unknown;
    amxd_status_t status = amxd_status_unknown_error;
    const char* family_str = gmap_ip_family_id2string(family);
    amxc_string_t tags;
    amxc_var_t old_addr_data;
    amxc_string_init(&tags, 0);
    amxc_var_init(&old_addr_data);

    when_str_empty_trace(ip_status, exit, ERROR, "Invalid argument");
    when_str_empty_trace(dev_mac, exit, ERROR, "Invalid argument");
    amxc_string_setf(&tags, "lan edev mac physical %s", family_str);

    status = gmap_ip_addressScope(family, dev_ip, &scope);
    when_failed_trace(status, exit, ERROR, "Error getting scope of '%s' %" PRIu32, dev_ip, family);

    // Safe to create the device because s_add_or_update_reachable_or_colliding_ip() is not for unreachable
    // IPs (we want to avoid creating a device while the device is not reachable anymore, because
    // the decive could be manually deleted)
    ok = gmap_devices_createDeviceOrGetKey(&dev_key, &already_exists, dev_mac,
                                           GMAP_DISCOVERY_SOURCE_DEVICE_ARP, amxc_string_get(&tags, 0), true, NULL, NULL);
    when_false_trace(ok, exit, ERROR, "Failed to create/get a gmap device");
    when_str_empty_trace(dev_key, exit, ERROR, "Device create/get did not yield key");
    if(already_exists) {
        ok = gmap_device_setTag(dev_key, family_str, NULL, gmap_traverse_this);
        if(!ok) {
            SAH_TRACEZ_ERROR(ME, "%s %s Error setting tag!", dev_key, dev_mac);
        }
    }

    status = gmap_ip_device_get_address(dev_key, family, dev_ip, &old_addr_data);
    if(status == amxd_status_ok) {
        bool old_reserved = GET_BOOL(&old_addr_data, "Reserved");
        status = gmap_ip_device_set_address(dev_key, family, dev_ip, NULL, ip_status, NULL, old_reserved);
        when_failed_trace(status, exit, ERROR, "Error setting address %s %s", dev_key, dev_ip);
    } else {
        const char* source = gmap_eth_dev_discoping_ip_disco_source(family, dev_ip);
        status = gmap_ip_device_add_address(dev_key, family, dev_ip, gmap_ip_address_scope2string(scope), ip_status, source, false);
        when_failed_trace(status, exit, ERROR, "Error adding address %s %s", dev_key, dev_ip);
    }

exit:
    amxc_var_clean(&old_addr_data);
    amxc_string_clean(&tags);
    free(dev_key);
}

/**
 * @implements discoping_up_cb_t
 *
 * Called when an IP was found to be up (not collision) according to ARP reply or gratuitous ARP.
 */
static void s_on_arp_up(const char* dev_ip, uint32_t family, const char* dev_mac, UNUSED void* userdata) {
    SAH_TRACEZ_INFO(ME, "Discoping up: %s %s", dev_ip, dev_mac);
    s_add_or_update_reachable_or_colliding_ip(dev_ip, family, dev_mac, GMAP_IP_STATUS_REACHABLE);
}

/**
 * @implements discoping_collision_cb_t
 *
 * Called when an IP was found to be claimed by 2 or more MAC addresses.
 */
static void s_on_arp_collision(const char* dev_ip, uint32_t family, const amxc_var_t* dev_mac_list,
                               UNUSED void* userdata) {

    SAH_TRACEZ_INFO(ME, "Discoping collision %s", dev_ip);

    amxc_var_for_each(mac_var, dev_mac_list) {
        const char* dev_mac = amxc_var_constcast(cstring_t, mac_var);
        s_add_or_update_reachable_or_colliding_ip(dev_ip, family, dev_mac, GMAP_IP_STATUS_ERROR);
    }
}

/**
 * @implements discoping_down_cb_t
 *
 * Called when an IP was found to be down according to ARP reply timeout
 */
static void s_on_arp_down(const char* dev_ip, uint32_t family, const char* dev_mac, UNUSED void* userdata) {
    SAH_TRACEZ_INFO(ME, "Discoping down %s %s", dev_ip, dev_mac);
    char* dev_key = NULL;
    const char* address_source = NULL;
    amxc_var_t ip_data;
    bool ip_owned = false;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_init(&ip_data);
    when_str_empty_trace(dev_ip, exit, ERROR, "NULL/Empty ip");
    when_str_empty_trace(dev_mac, exit, ERROR, "NULL/Empty mac");

    dev_key = gmap_devices_findByMac(dev_mac);

    when_null_trace(dev_key, exit, WARNING, "IP down but for unknown MAC %s %s (can legitimately happen after manual device deletion)", dev_ip, dev_mac);

    status = gmap_ip_device_get_address(dev_key, gmap_ip_family(dev_ip), dev_ip, &ip_data);
    when_true_trace(status == amxd_status_object_not_found, exit, INFO, "Unknown IP %s down for device %s (can legitimately happen, e.g. at startup)", dev_ip, dev_key);
    address_source = GETP_CHAR(&ip_data, "AddressSource");
    when_null_trace(address_source, exit, ERROR, "NULL address source %s %s", dev_key, dev_ip);

    // If the IP is detected/owned by discoping, delete it.
    // Otherwise, mark as unreachable (owner can then delete it, e.g. on dhcp lease expire)
    ip_owned = 0 == strcmp(address_source, GMAP_DISCOVERY_SOURCE_IP_AUTO)
        || 0 == strcmp(address_source, GMAP_DISCOVERY_SOURCE_IP_STATIC);
    if(ip_owned) {
        status = gmap_ip_device_delete_address(dev_key, family, dev_ip);
        when_failed_trace(status, exit, ERROR, "Failed deleting %s %s", dev_ip, dev_mac);
    } else {
        bool reserved = GET_BOOL(&ip_data, "Reserved");
        status = gmap_ip_device_set_address(dev_key, family, dev_ip, NULL,
                                            GMAP_IP_STATUS_NOT_REACHABLE, NULL, reserved);
        when_failed_trace(status, exit, ERROR, "Failed set %s %s unreachable", dev_ip, dev_mac);
    }

exit:
    free(dev_key);
    amxc_var_clean(&ip_data);
}

void gmap_eth_dev_discoping_new(gmap_eth_dev_discoping_t** discoping, amxo_parser_t* parser) {
    when_null_trace(discoping, error, ERROR, "NULL argument");
    when_null_trace(parser, error, ERROR, "NULL argument parser");
    *discoping = (gmap_eth_dev_discoping_t*) calloc(1, sizeof(gmap_eth_dev_discoping_t));
    when_null_trace(*discoping, error, ERROR, "Out of mem");

    discoping_new(&(*discoping)->lib_discoping, parser);
    when_null_trace((*discoping)->lib_discoping, error, ERROR, "Error initializing discoping");
    discoping_set_userdata((*discoping)->lib_discoping, *discoping);
    discoping_set_on_up_cb((*discoping)->lib_discoping, s_on_arp_up);
    discoping_set_on_down_cb((*discoping)->lib_discoping, s_on_arp_down);
    discoping_set_on_collision_cb((*discoping)->lib_discoping, s_on_arp_collision);

    return;
error:
    gmap_eth_dev_discoping_delete(discoping);
}

void gmap_eth_dev_discoping_delete(gmap_eth_dev_discoping_t** discoping) {
    if((discoping == NULL) || (*discoping == NULL)) {
        return;
    }

    discoping_delete(&(*discoping)->lib_discoping);
    free(*discoping);
    *discoping = NULL;
}

void gmap_eth_dev_discoping_open_socket(gmap_eth_dev_discoping_t* discoping, const char* netdev_name, const char* ip, const char* mac) {
    when_null_trace(discoping, exit, ERROR, "NULL argument");
    SAH_TRACEZ_INFO(ME, "Discoping open socket %s %s %s", netdev_name, ip, mac);
    discoping_open(discoping->lib_discoping, netdev_name, mac, ip);
exit:
    return;
}

void gmap_eth_dev_discoping_close_socket(gmap_eth_dev_discoping_t* discoping, const char* netdev_name, const char* ip, const char* mac) {
    when_null_trace(discoping, exit, ERROR, "NULL argument");
    SAH_TRACEZ_INFO(ME, "Discoping close socket %s %s %s", netdev_name, ip, mac);
    discoping_close(discoping->lib_discoping, netdev_name, mac, ip);
exit:
    return;
}

void gmap_eth_dev_discoping_verify(gmap_eth_dev_discoping_t* discoping, const char* ip, const char* mac) {
    when_null_trace(discoping, exit, ERROR, "NULL argument");
    SAH_TRACEZ_INFO(ME, "Discoping verify %s %s", ip, mac);
    discoping_verify(discoping->lib_discoping, ip, mac);
exit:
    return;
}

void gmap_eth_dev_discoping_verify_once(gmap_eth_dev_discoping_t* discoping, const char* ip, const char* mac) {
    when_null_trace(discoping, exit, ERROR, "NULL argument");
    SAH_TRACEZ_INFO(ME, "Discoping verify once %s %s", ip, mac);
    discoping_verify_once(discoping->lib_discoping, ip, mac);
exit:
    return;
}

void gmap_eth_dev_discoping_verify_if_unknown(gmap_eth_dev_discoping_t* discoping, const char* ip, const char* mac) {
    when_null_trace(discoping, exit, ERROR, "NULL argument");

    if(!discoping_is_watching(discoping->lib_discoping, ip, mac)) {
        discoping_verify(discoping->lib_discoping, ip, mac);
    }

exit:
    return;
}

void gmap_eth_dev_discoping_verify_all_unreachable_ips(gmap_eth_dev_discoping_t* discoping,
                                                       const char* dev_mac, const char* dev_key, uint32_t protocol_family) {
    amxc_var_t ips;
    amxc_var_init(&ips);
    when_null_trace(discoping, exit, ERROR, "NULL argument");
    when_null_trace(dev_mac, exit, ERROR, "NULL argument");
    when_null_trace(dev_key, exit, ERROR, "NULL argument");

    amxd_status_t status = gmap_ip_device_get_addresses(dev_key, &ips);
    when_failed_trace(status, exit, ERROR, "Error getting IPs of device %s", dev_key);
    amxc_var_for_each(ip_var, &ips) {
        const char* ip_address = GET_CHAR(ip_var, "Address");
        const char* ip_status = GET_CHAR(ip_var, "Status");
        if((ip_status == NULL) || (ip_address == NULL)) {
            SAH_TRACEZ_ERROR(ME, "Invalid address info");
        } else if((gmap_ip_family(ip_address) == protocol_family)
                  && (0 != strcmp(ip_status, GMAP_IP_STATUS_REACHABLE))) {
            gmap_eth_dev_discoping_verify(discoping, ip_address, dev_mac);
        }
    }

exit:
    amxc_var_clean(&ips);
}

void gmap_eth_dev_discoping_set_periodic_reverify_enabled(gmap_eth_dev_discoping_t* discoping, const char* ip, bool enabled) {
    when_null_trace(discoping, exit, ERROR, "NULL argument");

    SAH_TRACEZ_INFO(ME, "Discoping set periodic reverify enabled %s %d", ip, enabled);
    discoping_set_periodic_reverify_enabled(discoping->lib_discoping, ip, enabled);
exit:
    return;
}

void gmap_eth_dev_discoping_mark_seen_network(gmap_eth_dev_discoping_t* discoping, const char* ip, const char* mac) {
    when_null_trace(discoping, exit, ERROR, "NULL argument");

    SAH_TRACEZ_INFO(ME, "Discoping mark seen on network %s %s", ip, mac);
    discoping_mark_seen_network(discoping->lib_discoping, ip, mac);
exit:
    return;
}
