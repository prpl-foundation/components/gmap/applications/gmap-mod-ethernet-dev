/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_eth_dev_dhcp_option.h"
#include "gmap_eth_dev_dhcp.h"
#include "gmap_eth_dev.h"
#include "gmap_eth_dev_includes.h"
#include "gmap_eth_dev_util.h"
#include <amxb/amxb_subscribe.h>
#include <amxd/amxd_object_event.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxb/amxb.h>
#include <amxd/amxd_path.h>
#include <gmap/gmap_devices.h>
#include <gmap/extensions/ip/gmap_ext_ipaddress.h>
#include <string.h>
#include <stdlib.h>
#include <v4v6option.h>

/** Adds key-value entry with given key and given value to `target_var` */
static bool s_move_keyvalue(amxc_var_t* target_var, const char* key, amxc_var_t* move_value) {
    amxc_var_t* value_in_keyvalue = amxc_var_add_new_key(target_var, key);
    return 0 == amxc_var_move(value_in_keyvalue, move_value);
}

/** Convert DHCP Option 61 into which fields have to be set in gmap's device */
static void s_clientid_to_dev_fields(amxc_var_t* target_device_fields, amxc_var_t* option_parsed) {
    char* name = NULL;
    when_null_trace(target_device_fields, exit, ERROR, "NULL");
    when_null_trace(option_parsed, exit, ERROR, "NULL");

    name = amxc_var_dyncast(cstring_t, GETP_ARG(option_parsed, "ClientIdentifier"));
    gmap_eth_dev_util_str_to_upper(name);
    amxc_var_add_key(cstring_t, target_device_fields, "ClientID", name);

exit:
    free(name);
}

/** Convert DHCP Option 125 into which fields have to be set in gmap's device */
static void s_vendor_specific_info_to_dev_fields(amxc_var_t* target_device_fields, const char** target_device_tags, amxc_var_t* option_parsed) {
    const char* oui = NULL;
    const char* serial = NULL;
    const char* productClass = NULL;
    when_null_trace(target_device_fields, exit, ERROR, "NULL");
    when_null_trace(target_device_tags, exit, ERROR, "NULL");
    when_null_trace(option_parsed, exit, ERROR, "NULL");

    amxc_var_for_each(map, option_parsed) {
        amxc_var_t* data_list = GET_ARG(map, "Data");
        amxc_var_for_each(subcode, data_list) {
            uint32_t code = GET_UINT32(subcode, "Code");
            const char* value = GET_CHAR(subcode, "Data");
            switch(code) {
            case 1:     // oui
                oui = value;
                SAH_TRACEZ_INFO(ME, "Option 125 - code %d - OUI = %s", code, oui);
                break;
            case 2:     // serial
                serial = value;
                SAH_TRACEZ_INFO(ME, "Option 125 - code %d - serial = %s", code, serial);
                break;
            case 3:     // product class
                productClass = value;
                SAH_TRACEZ_INFO(ME, "Option 125 - code %d - productClass = %s", code, productClass);
                break;
            }
        }
    }

    amxc_var_add_key(cstring_t, target_device_fields, "SerialNumber", serial);
    amxc_var_add_key(cstring_t, target_device_fields, "ProductClass", productClass);
    amxc_var_add_key(cstring_t, target_device_fields, "OUI", oui);
    *target_device_tags = "manageable";

exit:
    return;
}

/** Convert DHCP Option 77 into which fields have to be set in gmap's device */
static void s_userclassid_to_dev_fields(amxc_var_t* target_device_fields, amxc_var_t* target_device_names, amxc_var_t* option_parsed) {
    const char* user_class_first_value = GETI_CHAR(option_parsed, 0);
    if(user_class_first_value != NULL) {
        amxc_var_add_key(cstring_t, target_device_fields, "UserClassID", user_class_first_value);
        amxc_var_add_key(cstring_t, target_device_names, "UserClassID", user_class_first_value);
    }
}

/** Convert DHCP Option 12 into which data has to be set in gmap's device */
static void s_hostname_to_dev_fields(amxc_var_t* target_device_fields UNUSED, amxc_var_t* target_device_names, amxc_var_t* option_parsed) {
    const char* name = amxc_var_constcast(cstring_t, option_parsed);
    when_null_trace(name, exit, ERROR, "NULL");
    amxc_var_add_key(cstring_t, target_device_names, "dhcp", name);
exit:
    return;
}

/** Convert a DHCP Option into which fields (and other info) have to be set in gmap's device */
void gmap_eth_dev_dhcp_option_to_dev_fields(amxc_var_t* target_device_fields,
                                            const char** target_device_tags,
                                            amxc_var_t* target_device_names,
                                            uint32_t option_tag, const char* option_raw_text) {

    unsigned char* option_raw_binary = NULL;
    uint32_t option_raw_binary_length = 0;
    amxc_var_t* option_parsed = NULL;
    when_null_trace(option_raw_text, exit, ERROR, "NULL");

    // parse raw data
    amxc_var_new(&option_parsed);
    when_null_trace(option_parsed, exit, ERROR, "Out of mem");
    option_raw_binary = dhcpoption_option_convert2bin(option_raw_text, &option_raw_binary_length);
    dhcpoption_v4parse(option_parsed, option_tag, option_raw_binary_length, option_raw_binary);
    when_false_trace(!amxc_var_is_null(option_parsed), exit, ERROR, "Error parsing DHCP option");

    switch(option_tag) {
    case 55:     // Parameter List
        s_move_keyvalue(target_device_fields, "DHCPOption55", option_parsed);
        break;
    case 60:     // Vendor Class ID
        s_move_keyvalue(target_device_fields, "VendorClassID", option_parsed);
        break;
    case 77:     // User Class ID
        s_userclassid_to_dev_fields(target_device_fields, target_device_names, option_parsed);
        break;
    case 61:     // Client ID
        s_clientid_to_dev_fields(target_device_fields, option_parsed);
        break;
    case 125:     // Vendor specific information
        s_vendor_specific_info_to_dev_fields(target_device_fields, target_device_tags, option_parsed);
        break;
    case 12:     // Host Name
        s_hostname_to_dev_fields(target_device_fields, target_device_names, option_parsed);
        break;
    default:
        SAH_TRACEZ_INFO(ME, "Option %d not handled", option_tag);
        break;
    }
exit:
    free(option_raw_binary);
    amxc_var_delete(&option_parsed);
}

void gmap_eth_dev_dhcp_option_on_add_or_change(const char* mac, uint32_t option_tag, const char* option_raw_text) {
    bool ok = false;
    bool already_exists = false;
    char* device_key = NULL;
    amxc_var_t device_fields;
    amxc_var_t device_names;
    const char* device_tags = NULL;
    amxc_var_init(&device_fields);
    amxc_var_init(&device_names);
    when_null_trace(mac, exit, ERROR, "NULL");
    when_null_trace(option_raw_text, exit, ERROR, "DHCP option raw text missing");

    // Create/get device:
    ok = gmap_devices_createDeviceOrGetKey(&device_key, &already_exists, mac, GMAP_DISCOVERY_SOURCE_DEVICE_DHCP,
                                           "lan edev mac physical ipv4 dhcp", true, NULL, NULL);
    when_false_trace(ok, exit, ERROR, "Failed to create/get a gmap device");
    when_str_empty_trace(device_key, exit, ERROR, "Device create/get did not yield key");

    if(already_exists) {
        ok = gmap_device_setTag(device_key, "ipv4 dhcp", NULL, gmap_traverse_this);
        if(!ok) {
            SAH_TRACEZ_ERROR(ME, "%s %s Error setting tag!", device_key, mac);
        }
    }

    amxc_var_set_type(&device_fields, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&device_names, AMXC_VAR_ID_HTABLE);
    gmap_eth_dev_dhcp_option_to_dev_fields(&device_fields, &device_tags, &device_names, option_tag, option_raw_text);
    if(!amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, &device_fields))) {
        gmap_device_set(device_key, &device_fields);
    }
    if(device_tags != NULL) {
        gmap_device_setTag(device_key, device_tags, NULL, gmap_traverse_this);
    }
    amxc_htable_for_each(it, amxc_var_constcast(amxc_htable_t, &device_names)) {
        const char* name_source = amxc_htable_it_get_key(it);
        const char* name = amxc_var_constcast(cstring_t, amxc_htable_it_get_data(it, amxc_var_t, hit));
        when_null_trace(name_source, exit, ERROR, "NULL");
        when_null_trace(name, exit, ERROR, "NULL");
        ok = gmap_device_setName(device_key, name, name_source);
        if(!ok) {
            SAH_TRACEZ_ERROR(ME, "Error setting name %s %s %s", device_key, name, name_source);
        }
    }

exit:
    amxc_var_clean(&device_fields);
    amxc_var_clean(&device_names);
    free(device_key);
    return;
}