/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_eth_dev_linker.h"
#include "gmap_eth_dev_bridgetable.h"
#include "gmap_eth_dev_includes.h"
#include "gmap_eth_dev_util.h"
#include <amxc/amxc.h>
#include <amxc/amxc_variant.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <stdbool.h>
#include <gmap/gmap.h>
#include <string.h>

#define LINK_DATASOURCE "mod-eth-dev"
#define ACTIVE_SOURCE_FROM_NETDEV "mod-eth-from-netdev"
#define ACTIVE_PRIORITY 50

struct gmap_eth_dev_linker {
    gmap_query_t* query;
};

static void s_try_link_in_gmap(const amxc_var_t* port, const char* device_key, const char* link_type) {
    const char* const port_key = GET_CHAR(port, "Key");
    bool ok = false;
    when_null_trace(port, exit, ERROR, "NULL");
    when_str_empty_trace(device_key, exit, ERROR, "EMPTY");
    when_str_empty_trace(port_key, exit, ERROR, "EMPTY");

    ok = gmap_devices_linkReplace(port_key, device_key, link_type, LINK_DATASOURCE, 0);
    when_false_trace(ok, exit, ERROR, "Failed to link %s<->%s", port_key, device_key);

exit:
    return;
}

static amxc_var_t* s_dev_fields(const amxc_var_t* port) {
    amxc_var_t* dev_fields = NULL;
    const char* port_name = "";
    const char* port_key = "";
    const char* port_layer1interface = "";
    amxc_var_new(&dev_fields);
    amxc_var_set_type(dev_fields, AMXC_VAR_ID_HTABLE);

    if(port != NULL) {
        port_key = GET_CHAR(port, "Key");
        port_name = GET_CHAR(port, "NetDevName");
        port_layer1interface = GET_CHAR(port, "Layer1Interface");
    }
    amxc_var_add_key(cstring_t, dev_fields, "InterfaceName", port_key);
    amxc_var_add_key(cstring_t, dev_fields, "Layer2Interface", port_name);
    amxc_var_add_key(cstring_t, dev_fields, "Layer1Interface", port_layer1interface);

    return dev_fields;
}

/**
 * Link all devices that should be linked to given port.
 *
 * Useful if port exists later in gmap than when @gmap_eth_dev_linker_link was called.
 *
 * @param port_gmap_fields: of type @ref AMXC_VAR_ID_HTABLE with entries of type @ref amxc_var_t.
 *   Parameters of port in gmap. Not NULL.
 *   Must have NetDevIndex field.
 */
static void s_on_new_gmap_port(gmap_eth_dev_linker_t* linker, const char* port_gmap_key, amxc_var_t* port_gmap_fields) {
    uint32_t port_netdev_index = 0;
    amxc_htable_t* macs_for_port = NULL;
    when_null_trace(linker, exit, ERROR, "NULL");
    when_str_empty_trace(port_gmap_key, exit, ERROR, "EMPTY");
    when_null_trace(port_gmap_fields, exit, ERROR, "NULL");
    port_netdev_index = GET_UINT32(port_gmap_fields, "NetDevIndex");
    when_false_trace(port_netdev_index != 0, exit, ERROR, "Port %s has NetDevIndex == 0", port_gmap_key);

    macs_for_port = gmap_eth_dev_bridgetable_macs_for_port(port_netdev_index);
    amxc_htable_for_each(it, macs_for_port) {
        const char* dev_mac = amxc_htable_it_get_key(it);
        char* dev_key = gmap_devices_findByMac(dev_mac);
        if(dev_key != NULL) {
            gmap_eth_dev_linker_link(linker, port_gmap_fields, port_netdev_index, dev_key);
            free(dev_key);
        }
    }

exit:
    amxc_htable_delete(&macs_for_port, variant_htable_it_free);
    return;
}

void gmap_eth_dev_linker_link(gmap_eth_dev_linker_t* linker, const amxc_var_t* port_fields_cached,
                              uint32_t port_netdev_idx, const char* dev_key) {
    const amxc_var_t* port_fields = NULL;
    amxc_var_t* port_fields_fresh = NULL;
    amxc_var_t* dev_fields = NULL;
    char* tags_list = NULL;
    const char* extra_tags = NULL;
    const char* link_type = NULL;
    when_null_trace(linker, exit, ERROR, "NULL");
    when_str_empty_trace(dev_key, exit, ERROR, "EMPTY");
    when_false_trace(port_netdev_idx != 0, exit, ERROR, "Invalid argument");

    // Do setActive regardless of linking, such that in case of problems with linking,
    // this does not propagate to also having problems with Active.
    gmap_device_setActive(dev_key, true, ACTIVE_SOURCE_FROM_NETDEV, ACTIVE_PRIORITY);

    if(port_fields_cached != NULL) {
        port_fields = port_fields_cached;
    } else {
        port_fields_fresh = gmap_eth_dev_util_get_gmap_device_fields(port_netdev_idx);
        // It's possible the port exists in netdev but not yet in gmap.
        // In that case, we cannot link in gmap yet, so exit here.
        when_null(port_fields_fresh, exit);
        port_fields = port_fields_fresh;
    }

    tags_list = amxc_var_dyncast(cstring_t, GET_ARG(port_fields, "Tags"));
    if(gmap_eth_dev_util_taglist_contains_tag(tags_list, "eth")) {
        link_type = "ethernet";
        extra_tags = "eth";
    } else if(gmap_eth_dev_util_taglist_contains_tag(tags_list, "wifi")) {
        link_type = "wifi";
        extra_tags = "wifi";
    }

    gmap_device_setTag(dev_key, extra_tags, NULL, gmap_traverse_this);

    dev_fields = s_dev_fields(port_fields);
    gmap_device_set(dev_key, dev_fields);

    s_try_link_in_gmap(port_fields, dev_key, link_type);

    /* Free now and not in exit, otherwise a double free is possible */
    amxc_var_delete(&port_fields_fresh);
    amxc_var_delete(&dev_fields);
    free(tags_list);

exit:
    return;
}

void gmap_eth_dev_linker_make_inactive(gmap_eth_dev_linker_t* linker, uint32_t port_netdev_idx, const char* dev_key) {
    amxc_var_t* dev_fields = NULL;
    when_null_trace(linker, exit, ERROR, "NULL");
    when_str_empty_trace(dev_key, exit, ERROR, "EMPTY");
    when_false_trace(port_netdev_idx != 0, exit, ERROR, "Invalid argument");

    gmap_device_clearTag(dev_key, "wifi eth", NULL, gmap_traverse_this);

    gmap_device_setActive(dev_key, false, ACTIVE_SOURCE_FROM_NETDEV, ACTIVE_PRIORITY);

exit:
    amxc_var_delete(&dev_fields);
    return;
}

/**
 * Called when a port comes into existence in gmap.
 *
 * Implements @ref gmap_query_cb_t
 */
static void s_on_query_result(gmap_query_t* query, const char* port_gmap_key,
                              amxc_var_t* port_gmap_fields, gmap_query_action_t action) {
    gmap_eth_dev_linker_t* linker = (gmap_eth_dev_linker_t*) query->data;
    when_null_trace(linker, exit, ERROR, "NULL");

    when_false_trace(action != gmap_query_error, exit, ERROR, "Error on query (%s)", port_gmap_key);
    when_true(action != gmap_query_expression_start_matching, exit);

    s_on_new_gmap_port(linker, port_gmap_key, port_gmap_fields);

exit:
    return;
}

void gmap_eth_dev_linker_new(gmap_eth_dev_linker_t** linker,
                             const gmap_eth_dev_bridge_t* bridge) {
    int status = -1;
    const char* mac = NULL;
    amxc_string_t expression;
    amxc_string_t query_name;

    amxc_string_init(&expression, 0);
    amxc_string_init(&query_name, 0);

    when_null_trace(linker, exit, ERROR, "NULL");
    when_null_trace(bridge, exit, ERROR, "NULL");
    *linker = (gmap_eth_dev_linker_t*) calloc(1, sizeof(gmap_eth_dev_linker_t));
    when_null_trace(*linker, exit, ERROR, "Out of mem");

    mac = gmap_eth_dev_bridge_get_mac_address(bridge);
    when_str_empty_trace(mac, exit, ERROR, "NULL/Empty MAC. Linking will be unreliable.");

    status = amxc_string_setf(&expression, "self interface && (eth || wifi) && .NetDevIndex != 0 && .PhysAddress ^= \"%s\"", mac);
    when_failed_trace(status, exit, ERROR, "Failed to build expression. Linking will be unreliable.");
    status = amxc_string_setf(&query_name, "port-to-link-dev-%s", mac);
    when_failed_trace(status, exit, ERROR, "Failed to build expression. Linking will be unreliable.");

    gmap_query_open_ext_2(&(*linker)->query,
                          amxc_string_get(&expression, 0),
                          amxc_string_get(&query_name, 0),
                          GMAP_QUERY_IGNORE_DEVICE_UPDATED,
                          s_on_query_result, *linker);
    if((*linker)->query == NULL) {
        SAH_TRACEZ_ERROR(ME, "Error: cannot init gmap query. Linking will be unreliable.");
    }


exit:
    amxc_string_clean(&expression);
    amxc_string_clean(&query_name);
    return;
}

void gmap_eth_dev_linker_delete(gmap_eth_dev_linker_t** linker) {
    if((linker == NULL) || (*linker == NULL)) {
        return;
    }

    if((*linker)->query != NULL) {
        gmap_query_close((*linker)->query);
    }

    free(*linker);
    *linker = NULL;
}
